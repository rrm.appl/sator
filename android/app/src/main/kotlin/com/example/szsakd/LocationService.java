package com.example.szsakd;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.HashMap;

public class LocationService extends Service {

    private Context context;
    NotificationManager manager;

    private LocationRequest locationRequest;
    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null) {
                return;
            }
            if (locationResult.getLastLocation() != null) {
                HashMap<String, Double> position = new HashMap<String, Double>();
                position.put("latitude",locationResult.getLastLocation().getLatitude());
                position.put("longitude",locationResult.getLastLocation().getLongitude());
                ConstData.latilongi_list.add(position);
                Log.i("location", "LOC: " +locationResult.getLastLocation().getLatitude() + " " + locationResult.getLastLocation().getLongitude());
            }
        }
    };

    void startService(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            manager = getSystemService(NotificationManager.class);
        }
        context = this;

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5 * 1000);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) { }
        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());

        startForeground(101,getNotif("-1"));
    }

    void stopService(){
        LocationServices.getFusedLocationProviderClient(context).removeLocationUpdates(locationCallback);
        stopForeground(true);
        stopSelf();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent!=null){
            String action = intent.getAction();
            if(action!=null){
                if(action.equals(ConstData.START_SERVICE)){
                    startService();
                }
                if(action.equals(ConstData.STOP_SERVICE)){
                    stopService();
                }
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    Notification getNotif(String text){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"messages")
                .setContentTitle("Kirándulás rögzítése")
                .setContentText("Kirándulás rögzítése folyamatban...")
                .setSmallIcon(R.drawable.logo);
        return  builder.build();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
