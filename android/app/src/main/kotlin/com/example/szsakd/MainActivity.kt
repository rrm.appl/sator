package com.example.szsakd

import android.R
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.Nullable
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.android.SplashScreen
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodChannel


class MainActivity: FlutterActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }

    override fun provideSplashScreen(): io.flutter.embedding.android.SplashScreen? {
        return SplashScreen()
    }

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        ConstData.mainActivity = this;

        MethodChannel(getFlutterView(), "com.rrm.szakd").setMethodCallHandler { call, result ->
            if (call.method == "startService") {
                ConstData.isRunning = true
                ConstData.latilongi_list.clear()
                var intent : Intent = Intent( this, LocationService::class.java)
                intent.action = ConstData.START_SERVICE;
                startService(intent)
            }
            if (call.method == "stopService") {
                synchronized(ConstData.isRunning) {
                    ConstData.isRunning = false
                }
                var intent : Intent = Intent( this, LocationService::class.java)
                intent.action = ConstData.STOP_SERVICE
                startServieF(intent)
                result.success(ConstData.latilongi_list)
            }
        }
    }

    private fun getFlutterView(): BinaryMessenger? {
        return flutterEngine!!.dartExecutor.binaryMessenger
    }
    private fun startServieF(intent : Intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            applicationContext.startForegroundService(intent)
        } else {
            startService(intent)
        }
    }
}

