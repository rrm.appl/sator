package com.example.szsakd

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.Nullable


class SplashScreen : io.flutter.embedding.android.SplashScreen {
    @Nullable
    override fun createSplashView(context: Context, @Nullable savedInstanceState: Bundle?): View {
        return LayoutInflater.from(context).inflate(R.layout.splash_screen, null, false)
    }

    override fun transitionToFlutter(onTransitionComplete: Runnable) {
        onTransitionComplete.run()
    }
}