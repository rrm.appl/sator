import 'package:flutter/material.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/main_page.dart';

void main() async {
  await Config.initFlutter();
  await Config.initFirebase();
  Config.setFireStorePersistence(false);
  Config.setFirebaseMessaging();
  Config.setFirebaseStorage();
  Config.setNotification();
  runApp(MainPage());
}
