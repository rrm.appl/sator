import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:szsakd/bloc/auth/auth_event.dart';
import 'package:szsakd/bloc/auth/auth_state.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/model/user_model.dart';
import 'package:szsakd/repos/auth/auth_repository.dart';

class AuthBloc extends Bloc<AuthEvent,AuthState> {

  final AuthRepository _authRepository;
  AuthBloc(this._authRepository) : super(Uninitialized());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is StartingApp) {
      yield* _mapAppStartedToState();
    } else if (event is SigningUp) {
      yield* _mapSinginUpToState(event);
    } else if (event is LoggingIn) {
      yield* _mapLoggingInToState(event);
    } else if (event is LoggingOut) {
      yield* _mapLoggingOutToState();
    } else if(event is ChangeUserData){
      yield* _mapChangeUserDataToState(event.userData);
    }
  }

  Stream<AuthState> _mapChangeUserDataToState(FireStoreUser userData) async* {
    debugPrint("asdsad");
    //yield LoadingAuth();
    yield UserDataChange(userData: userData);
  }


  Stream<AuthState> _mapAppStartedToState() async* {
    try {
      yield LoadingAuth();
      final isSignedIn = await _authRepository.isSignedIn();
      if (isSignedIn) {
        User user = await _authRepository.getUser();
        FireStoreUser fireStoreUser = await _authRepository.readUserFromLocal(user);
        Config.currentUser = fireStoreUser;
        Config.currentFirebaseUser = user;
        debugPrint(user.email);
        yield Authenticated(fireStoreUser);
      } else {
        yield Unauthenticated();
      }
    } catch (_) {
      yield Unauthenticated();
    }
  }

  Stream<AuthState> _mapLoggingInToState(LoggingIn event) async* {
    try {
      yield LoadingAuth();
      String error = '';
      User user = await _authRepository.signInWithCredentials(event.email, event.password).catchError((e){
        error = e;
      });
      if(error == 'user-not-found'){
        yield NotLoggedIn(error: 'Ez az e-mail cim nincs még regisztrálva.');
      }else if(error == 'wrong-password'){
        yield NotLoggedIn(error: 'Hibás jelszó.');
      }else if(!user.emailVerified){
        yield NotLoggedIn(error: 'Az email cime nincs érvényesitve.' );
      } else {
        debugPrint("authok");
        debugPrint("uid: "+user.uid);
        FireStoreUser fireStoreUser = await _authRepository.readUserFromCloud(user.uid);
        await _authRepository.saveUserFromLocal(fireStoreUser);

        bool refreshToken = await _authRepository.refreshToken(fireStoreUser);
        if(refreshToken){
          fireStoreUser = await _authRepository.readUserFromLocal(user);
        }

        yield Authenticated(fireStoreUser);
      }
    } catch (_) {
      yield Unauthenticated();
    }
  }

  Stream<AuthState>_mapSinginUpToState(SigningUp event) async* {
    try {
      yield LoadingAuth();
      String error = '';
      User user = await _authRepository.createUserWithEmailAndPassword(event.fireStoreUser.email, event.password).catchError(
          (e){
            error = e;
          }
      );
      if(error == 'email-already-in-use'){
        yield NotSignedUp(error: 'Ezzel az e-email cimmel már van regisztrált fiók.');
      } else{
        event.fireStoreUser.uid = user.uid;
        await _authRepository.createUserIntoFireStore(event.fireStoreUser);
        if(!Config.currentFirebaseUser.emailVerified) {
          _authRepository.signOut();
          yield NotSignedUp(error: 'Emailben kapott lerítak alapján érvényesítse email címét.');
        }
        else {
          yield Authenticated(event.fireStoreUser);
        }
      }

    } catch (_) {
      yield Unauthenticated();
    }
  }

  Stream<AuthState> _mapLoggingOutToState() async* {
    try {
      yield LoadingAuth();
      await _authRepository.signOut();
      yield Unauthenticated();
    } catch (e) {
      print(e);
    }
  }

}