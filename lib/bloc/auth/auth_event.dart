import 'package:equatable/equatable.dart';
import 'package:szsakd/model/user_model.dart';

abstract class AuthEvent extends Equatable{
  const AuthEvent();

  @override
  List<Object> get props => [];
}

//
class StartingApp extends AuthEvent {
  StartingApp();
}

class LoggingIn extends AuthEvent {
  String email;
  String password;
  LoggingIn({this.email,this.password});
  @override
  String toString() => 'LoggedIn';
}

class LoggingOut extends AuthEvent {
  LoggingOut();
  @override
  String toString() => 'LoggedOut';
}

//
class SigningUp extends AuthEvent{
  FireStoreUser fireStoreUser;
  String password;
  SigningUp({this.fireStoreUser,this.password});
}
//

class ChangeUserData extends AuthEvent{
  FireStoreUser userData;
  ChangeUserData({this.userData});
}
