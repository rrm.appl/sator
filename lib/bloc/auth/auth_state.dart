import 'package:equatable/equatable.dart';
import 'package:szsakd/model/user_model.dart';

abstract class AuthState extends Equatable{
  const AuthState();

  @override
  List<Object> get props => [];
}

class LoadingAuth extends AuthState {
  const LoadingAuth();
  @override
  String toString() => 'LoadingAuth';
}

//
class Uninitialized extends AuthState {
  const Uninitialized();
  @override
  String toString() => 'Uninitialized';
}

class Authenticated extends AuthState {
  final FireStoreUser fireStoreUser;
  Authenticated(this.fireStoreUser);

  @override
  String toString() => 'Authenticated { displayName: ${fireStoreUser.email} }';
}

class Unauthenticated extends AuthState {
  String error;
  FireStoreUser fireStoreUser;
  Unauthenticated({this.error,this.fireStoreUser});
  @override
  String toString() => 'Unauthenticated';
}

class NotSignedUp extends AuthState {
  String error;
  NotSignedUp({this.error});
  @override
  String toString() => 'Unauthenticated';
}

class NotLoggedIn extends AuthState {
  String error;
  NotLoggedIn({this.error});
  @override
  String toString() => 'Unauthenticated';
}
//

//
class AppStarted extends AuthState {
  const AppStarted();
  @override
  String toString() => 'AppStarted';
}

class LoggedIn extends AuthState {
  const LoggedIn();
  @override
  String toString() => 'LoggedIn';
}

class LoggedOut extends AuthState {
  const LoggedOut();
  @override
  String toString() => 'LoggedOut';
}

//
class UserDataChange extends AuthState{
  FireStoreUser userData;
  UserDataChange({this.userData});
}