import 'package:equatable/equatable.dart';

class RankEvent extends Equatable{
  const RankEvent();

  @override
  List<Object> get props => [];
}


class GetTeams extends RankEvent{
  GetTeams();
}
class GetTeamRanks extends RankEvent{
  GetTeamRanks();
}
class UpdateRating extends RankEvent{
  String uid;
  UpdateRating({this.uid});
}
