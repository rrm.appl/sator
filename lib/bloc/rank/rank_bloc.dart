import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:szsakd/bloc/rank/rank_event.dart';
import 'package:szsakd/bloc/rank/rank_state.dart';
import 'package:szsakd/model/team_model.dart';
import 'package:szsakd/model/team_rank_model.dart';
import 'package:szsakd/repos/rank/rank_repository.dart';


class RankBloc extends Bloc<RankEvent,RankState> {

  RankRepository rankRepository;
  RankBloc(this.rankRepository) : super(UninitializedState());


  @override
  Stream<RankState> mapEventToState(RankEvent event)  async*{
      if(event is GetTeams){
        yield* _mapGetTeamsToState(event);
      }else if(event is GetTeamRanks){
        yield* _mapGetTeamRanksToState(event);
      }
  }

  @override
  Stream<RankState> _mapGetTeamsToState(GetTeams event)  async* {
    yield RankStateLoadingState();
    try{
      List<Team> teams = await rankRepository.getTeams();
      yield TeamLoadedState(teams: teams);
    }catch(e){
      print(e);
    }
  }

  @override
  Stream<RankState> _mapGetTeamRanksToState(GetTeamRanks event)  async* {
    yield RankStateLoadingState();
    try{
      List<TeamRank> teamRanks = await rankRepository.getTeamRanks();
      yield TeamRanksLoadedState(teamRanks: teamRanks);
    }catch(e){
      print(e);
    }
  }
}