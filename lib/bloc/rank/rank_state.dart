import 'package:equatable/equatable.dart';
import 'package:szsakd/model/team_model.dart';
import 'package:szsakd/model/team_rank_model.dart';

abstract class RankState extends Equatable{
  const RankState();

  @override
  List<Object> get props => [];
}

class UninitializedState extends RankState{
  const UninitializedState();
}

class RankStateLoadingState extends RankState{
  const RankStateLoadingState();
}

class TeamLoadedState extends RankState{
  List<Team> teams;
  TeamLoadedState({this.teams});
}

class TeamRanksLoadedState extends RankState{
  List<TeamRank> teamRanks;
  TeamRanksLoadedState({this.teamRanks});
}

