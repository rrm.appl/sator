
import 'package:szsakd/model/hiking_model.dart';

class HikingEvent{
  const HikingEvent();
}

//local
class UpdateHikingLocal extends HikingEvent{
  Hiking hiking;
  UpdateHikingLocal({this.hiking});
}

class GetHikingLocal extends HikingEvent{
  GetHikingLocal();
}

class DeleteHikingLocal extends HikingEvent{
  DeleteHikingLocal();
}

//cloud
class UpdateHikingCloud extends HikingEvent{
  Hiking hiking;
  UpdateHikingCloud({this.hiking});
}

class GetHikingCloud extends HikingEvent{
  GetHikingCloud();
}