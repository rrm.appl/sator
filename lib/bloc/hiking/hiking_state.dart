import 'package:szsakd/model/hiking_model.dart';

class HikingState{
  const HikingState();

  Hiking get hiking => null;
}

class UninitializedState extends HikingState{
  const UninitializedState();
}

//local
class HikingLocalLoadingState extends HikingState{
  const HikingLocalLoadingState();
}

class HikingLocalLoadedState extends HikingState{
  Hiking hiking;
  bool delete;
  HikingLocalLoadedState({this.hiking, this.delete});
}

//cloud
class HikingCloudLoadingState extends HikingState{
  const HikingCloudLoadingState();
}

class HikingCloudLoadedState extends HikingState{
  List<Hiking> hikings;
  HikingCloudLoadedState({this.hikings});
}