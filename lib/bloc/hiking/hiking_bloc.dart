import 'package:bloc/bloc.dart';
import 'package:szsakd/bloc/hiking/hiking_event.dart';
import 'package:szsakd/bloc/hiking/hiking_state.dart';
import 'package:szsakd/model/hiking_model.dart';
import 'package:szsakd/repos/hiking/hiking_repository.dart';

class HikingBloc extends Bloc<HikingEvent,HikingState> {

  HikingRepository hikingRepository;
  HikingBloc(this.hikingRepository) : super(UninitializedState());
  @override
  Stream<HikingState> mapEventToState(HikingEvent event)  async*{
    if(event is UpdateHikingLocal){
      yield* _mapUpdateLocalHikingToState(event);
    }
    if(event is GetHikingLocal){
      yield* _mapGetLocalHikingToState(event);
    }
    if(event is DeleteHikingLocal){
      yield* _mapDeleteLocalHikingToState(event);
    }

    if(event is UpdateHikingCloud){
      yield* _mapUpdateCloudHikingToState(event);
    }
    if(event is GetHikingCloud){
      yield* _mapGetCloudHikingToState(event);
    }
  }

  @override
  Stream<HikingState> _mapUpdateLocalHikingToState(UpdateHikingLocal event)  async* {
    yield HikingLocalLoadingState();
    try{
      await hikingRepository.saveHikingLocal(event.hiking);
      yield HikingLocalLoadedState(delete: false);
    }catch(e){
      print(e);
    }
  }

  @override
  Stream<HikingState> _mapGetLocalHikingToState(GetHikingLocal event)  async* {
    yield HikingLocalLoadingState();
    try{
      final hiking = await hikingRepository.getHikingLocal();
      yield HikingLocalLoadedState(hiking: hiking, delete: false);
    }catch(e){
      print(e);
    }
  }

  @override
  Stream<HikingState> _mapDeleteLocalHikingToState(DeleteHikingLocal event)  async* {
    yield  HikingLocalLoadingState();
    try{
      await hikingRepository.deleteHikingLocal();
      yield HikingLocalLoadedState(delete: true);
    }catch(e){
      print(e);
    }

  }

  @override
  Stream<HikingState> _mapUpdateCloudHikingToState(UpdateHikingCloud event)  async* {
    yield HikingCloudLoadingState();
    try{
      await hikingRepository.saveHikingCloud(event.hiking);
      await hikingRepository.deleteHikingLocal();
      yield HikingCloudLoadedState();
    }catch(e){
      print(e);
    }
  }

  @override
  Stream<HikingState> _mapGetCloudHikingToState(GetHikingCloud event)  async* {
    yield HikingCloudLoadingState();
    try{
      List<Hiking> hikings = await hikingRepository.getHikingCloud();
      yield HikingCloudLoadedState(hikings: hikings);
    }catch(e){
      print(e);
    }
  }
}