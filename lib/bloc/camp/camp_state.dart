import 'package:equatable/equatable.dart';
import 'package:szsakd/model/camp_model.dart';

abstract class CampState extends Equatable{
  List<Camp> camps;
  CampState({this.camps});

  @override
  List<Object> get props => [];
}

class UninitializedState extends CampState{
  UninitializedState();
}

//local
class CampLocalLoadingState extends CampState{
  CampLocalLoadingState();
}

class CampLocalLoadedState extends CampState{
  Camp camp;
  bool delete;
  CampLocalLoadedState({this.camp, this.delete});
}

//cloud
class CampCloudLoadingState extends CampState{
  CampCloudLoadingState();
}

class CampCloudLoadedState extends CampState{
  List<Camp> camps;
  CampCloudLoadedState({this.camps});
}