import 'package:equatable/equatable.dart';
import 'package:szsakd/model/camp_model.dart';

class CampEvent extends Equatable{
  const CampEvent();

  @override
  List<Object> get props => [];
}

//local
class UpdateCampLocal extends CampEvent{
    Camp camp;
    UpdateCampLocal({this.camp});
}

class GetCampLocal extends CampEvent{
  GetCampLocal();
}

class DeleteCampLocal extends CampEvent{
  DeleteCampLocal();
}

//cloud
class UpdateCampCloud extends CampEvent{
  Camp camp;
  UpdateCampCloud({this.camp});
}

class GetCampCloud extends CampEvent{
  GetCampCloud();
}

class ChangeCampCloud extends CampEvent{
  List<Camp> camps;
  ChangeCampCloud({this.camps});
}