import 'package:bloc/bloc.dart';
import 'package:szsakd/bloc/camp/camp_event.dart';
import 'package:szsakd/bloc/camp/camp_state.dart';
import 'package:szsakd/model/camp_model.dart';
import 'package:szsakd/repos/camp/camp_repository.dart';

class CampBloc extends Bloc<CampEvent,CampState> {

  CampRepository campRepository;
  CampBloc(this.campRepository) : super(CampLocalLoadedState());

  @override
  CampState get initialState  => CampLocalLoadingState();

  @override
  Stream<CampState> mapEventToState(CampEvent event)  async*{
      if(event is UpdateCampLocal){
        yield* _mapUpdateLocalCampToState(event);
      }
      if(event is GetCampLocal){
        yield* _mapGetLocalCampToState(event);
      }
      if(event is DeleteCampLocal){
        yield* _mapDeleteLocalCampToState(event);
      }
      if(event is UpdateCampCloud){
        yield* _mapUpdateCloudCampToState(event);
      }
      if(event is GetCampCloud){
        yield* _mapGetCloudCampToState(event);
      }
      if(event is ChangeCampCloud){
        yield* _mapChangesCampToState(event);
      }
  }

  @override
  Stream<CampState> _mapUpdateLocalCampToState(UpdateCampLocal event)  async* {
    yield CampLocalLoadingState();
    try{
      await campRepository.saveCampLocal(event.camp);
    }catch(e){
      print(e);
    }
    yield CampLocalLoadedState(delete: false);
  }

  @override
  Stream<CampState> _mapGetLocalCampToState(GetCampLocal event)  async* {
    yield CampLocalLoadingState();
    try{
      final camp = await campRepository.getCampLocal();
      yield CampLocalLoadedState(camp: camp, delete: false);
    }catch(e){
      print(e);
    }
  }

  @override
  Stream<CampState> _mapDeleteLocalCampToState(DeleteCampLocal event)  async* {
    yield CampLocalLoadingState();
    try{
      await campRepository.deleteCampLocal();
      yield CampLocalLoadedState(delete: true);
    }catch(e){
      print(e);
    }
  }

  @override
  Stream<CampState> _mapUpdateCloudCampToState(UpdateCampCloud event)  async* {
    yield CampCloudLoadingState();
    try{
      await campRepository.saveCampCloud(event.camp);
      await campRepository.deleteCampLocal();
      yield CampCloudLoadedState();
    }catch(e){
      print(e);
    }
  }

  @override
  Stream<CampState> _mapGetCloudCampToState(GetCampCloud event)  async* {
    yield CampCloudLoadingState();
    try{
      List<Camp> camps = await campRepository.getCampCloud();
      yield CampCloudLoadedState(camps: camps);
    }catch(e){
      print(e);
    }
  }

  @override
  Stream<CampState> _mapChangesCampToState(ChangeCampCloud event)  async* {
    yield CampCloudLoadingState();
    yield CampCloudLoadedState(camps: event.camps);
  }
}