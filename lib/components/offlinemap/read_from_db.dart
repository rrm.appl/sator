import 'dart:developer';
import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:ui' as UI;
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/src/layer/tile_provider/tile_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;

class DbTile extends TileProvider {
  File dbFile;
  String dbFilePath;
  Future<Database> database;
  Database _loadedDb;
  bool isDisposed = false;

  DbTile._({this.dbFilePath}) {
    database = _loadMBTilesDatabase();
  }

  factory DbTile.fromFile(String dbFilePath) =>
      DbTile._(dbFilePath: dbFilePath);

  Future<Database> _loadMBTilesDatabase() async {
    if (_loadedDb == null) {

      final externalDirectory = await pathProvider.getExternalStorageDirectory();
      dbFile = File('${externalDirectory.path}/'+dbFilePath);
      _loadedDb = await openDatabase('${externalDirectory.path}/'+dbFilePath);

      if (isDisposed) {
        await _loadedDb.close();
        _loadedDb = null;
        throw Exception('Tileprovider is already disposed');
      }
    }

    return _loadedDb;
  }

  @override
  void dispose() {
    if (_loadedDb != null) {
      _loadedDb.close();
      _loadedDb = null;
    }
    isDisposed = true;
  }

  @override
  ImageProvider getImage(Coords<num> coords, TileLayerOptions options) {
    var x = coords.x.round();
    var y = options.tms
        ? invertY(coords.y.round(), coords.z.round())
        : coords.y.round();
    var z = coords.z.round();
    log("getImage: "+coords.z.toString()+"/"+coords.x.toString()+"/"+coords.y.toString());
    return DBTileImage(
      database,
      Coords<int>(x, y)..z = z
    );
  }
}

class DBTileImage extends ImageProvider<DBTileImage> {
  final Future<Database> database;
  final Coords<int> coords;

  DBTileImage(this.database, this.coords);

  @override
  ImageStreamCompleter load(DBTileImage key, decode) {
    return MultiFrameImageStreamCompleter(
        codec: _loadAsync(key),
        scale: 1,
        informationCollector: () sync* {
          yield DiagnosticsProperty<ImageProvider>('Image provider', this);
          yield DiagnosticsProperty<ImageProvider>('Image key', key);
        });
  }

  Future<UI.Codec> _loadAsync(DBTileImage key) async {
    assert(key == this);
    log('${coords.z} ${coords.x} ${coords.y} ');
    final db = await key.database;
    //List<Map> result = await db.rawQuery('select * from tiles');
    List<Map> result = await db.rawQuery('select tile_data from tiles '
        'where zoom_level = ${coords.z} AND '
        'tile_column = ${coords.x} AND '
        'tile_row = ${coords.y} limit 1');
    Uint8List bytes;
    if(result.isNotEmpty){
      var list = json.decode(result.first['tile_data']);
      List<int> lista = new List<int>.from(list);
      bytes = Uint8List.fromList(lista);
    }

    if (bytes == null) {
      return Future<UI.Codec>.error('Failed to load tile for coords: $coords');
    }
    return await PaintingBinding.instance.instantiateImageCodec(bytes);
  }

  @override
  Future<DBTileImage> obtainKey(ImageConfiguration configuration) {
    return SynchronousFuture(this);
  }

  @override
  int get hashCode => coords.hashCode;

  @override
  bool operator ==(other) {
    return other is DBTileImage &&
        coords == other.coords;
  }
}