import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:szsakd/bloc/auth/auth_bloc.dart';
import 'package:szsakd/bloc/auth/auth_event.dart';
import 'package:szsakd/bloc/auth/auth_state.dart';
import 'package:szsakd/bloc/rank/rank_bloc.dart';
import 'package:szsakd/bloc/rank/rank_event.dart';
import 'package:szsakd/bloc/rank/rank_state.dart';
import 'package:szsakd/components/auth/singin_page.dart';
import 'package:szsakd/components/common/rounded_button.dart';
import 'package:szsakd/components/common/dialog_pop_up.dart';
import 'package:szsakd/components/main/main_map_page.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/model/team_model.dart';
import 'package:szsakd/model/user_model.dart';
import 'package:szsakd/utils/colors.dart';
import 'package:szsakd/utils/validators.dart';

class RegisterPage extends StatefulWidget{
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage>{

  TextEditingController _nameContorller = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordControler = TextEditingController();
  AuthBloc _authBloc;
  bool visiblePassword;

  Team actDropDwon;
  List<Team> teams;
  @override
  void initState() {
    _authBloc = BlocProvider.of<AuthBloc>(context);
    BlocProvider.of<RankBloc>(context)..add(GetTeams());
    visiblePassword = true;
    teams = [];
  }
  @override
  Widget build(BuildContext context) {
    final pice = MediaQuery.of(context).size.height/10;
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: WillPopScope(
        child: SafeArea(
          child: Container(
            child: BlocConsumer<RankBloc, RankState>(
                listener: (context, state){
                  if(state is TeamLoadedState){
                    if(state.teams!=null){
                      setState(() {
                        teams = state.teams;
                      });
                    }
                  }
                },
                builder: (context, state){
                  if(state is RankStateLoadingState){
                    return Center(child: CircularProgressIndicator());
                  }
                  if(state is TeamLoadedState){
                    return BlocConsumer<AuthBloc, AuthState>(
                        listener: (context, state){
                          if(state is Authenticated){
                            Config.currentUser = state.fireStoreUser;
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => MainMapPage(fireStoreUser: state.fireStoreUser)));
                          }
                          if(state is NotSignedUp){
                            if(state.error != null) {
                              showErrorSignUp(context, state.error);
                            }
                          }
                        },
                        builder: (context, state){
                          if(state is LoadingAuth){
                            return Center(child: CircularProgressIndicator());
                          }
                          if(state is Unauthenticated || state is NotSignedUp){
                            return SingleChildScrollView(
                              reverse: true,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(32.0),
                                    child: Center(
                                        child: ConstrainedBox(
                                          constraints: BoxConstraints(
                                              maxHeight: MediaQuery.of(context).size.height/9
                                          ),
                                          child: Image(
                                              image: AssetImage('assets/logo.png')
                                          ),
                                        )
                                    ),
                                  ),
                                  Align(
                                      alignment: Alignment.center,
                                      child: Text( 'Regisztráció',style: TextStyle(color: WHITE_COLOR),textScaleFactor: 1.5,)
                                  ),
                                  SizedBox(height: pice/2,),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextField(
                                      controller: _nameContorller,
                                      style: TextStyle(color: WHITE_COLOR),
                                      textAlign: TextAlign.left,
                                      keyboardType: TextInputType.text,
                                      decoration: InputDecoration(
                                        hintText: 'Név...',
                                        hintStyle: TextStyle(color: WHITER_COLOR),
                                        contentPadding: EdgeInsets.all(16),
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: WHITER_COLOR),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: WHITE_COLOR),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: DropdownButton<Team>(
                                      dropdownColor: PRIMARY_COLOR,
                                      style: TextStyle(color: WHITE_COLOR),
                                      value: actDropDwon,
                                      onChanged: (Team newValue) {
                                        setState(() {
                                          actDropDwon = newValue;
                                        });
                                      },
                                      items: teams.map<DropdownMenuItem<Team>>((Team value) {
                                        return DropdownMenuItem<Team>(
                                          value: value,
                                          child: Text(value.name),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      controller: _emailController,
                                      validator: (value){
                                        if(Validators.isValidEmail(value)){
                                          return null;
                                        }else{
                                          return 'Helytelen e-mail cim formátum.';
                                        }
                                      },
                                      autovalidateMode: AutovalidateMode.always,
                                      style: TextStyle(color: WHITE_COLOR),
                                      textAlign: TextAlign.left,
                                      keyboardType: TextInputType.text,
                                      decoration: InputDecoration(
                                        hintText: 'E-mail...',
                                        hintStyle: TextStyle(color: WHITER_COLOR),
                                        contentPadding: EdgeInsets.all(16),
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: WHITER_COLOR),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: WHITE_COLOR),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      controller: _passwordControler,
                                      validator: (value){
                                        if(Validators.isValidPassword(value)){
                                          return null;
                                        }else{
                                          return 'Minimum 8 hosszu, 1 kis betü, 1 nagy betü, 1 szám.';
                                        }
                                      },
                                      autovalidateMode: AutovalidateMode.always,
                                      style: TextStyle(color: WHITE_COLOR),
                                      textAlign: TextAlign.left,
                                      keyboardType: TextInputType.text,
                                      obscureText: visiblePassword,
                                      decoration: InputDecoration(
                                          hintText: 'Jelszó...',
                                          hintStyle: TextStyle(color: WHITER_COLOR),
                                          contentPadding: EdgeInsets.all(16),
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(color: WHITER_COLOR),
                                          ),
                                          focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(color: WHITE_COLOR),
                                          ),
                                          suffixIcon: GestureDetector(
                                            child: Icon(Icons.visibility,color: WHITE_COLOR),
                                            onLongPress: (){
                                              setState(() {
                                                visiblePassword = false;
                                              });
                                            },
                                            onLongPressUp: (){
                                              setState(() {
                                                visiblePassword = true;
                                              });
                                            },
                                          )
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: pice/2,),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: ConstrainedBox(
                                      constraints: BoxConstraints(
                                          minWidth: MediaQuery.of(context).size.width/3
                                      ),
                                      child: RoundedButton(
                                          title: 'Regisztráció',
                                          color: TENT_COLOR,
                                          onPressed: register
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: pice/2,),
                                  Row(
                                      children: <Widget>[
                                        Expanded(
                                            child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Divider(
                                                color: Colors.lightBlue,
                                              ),
                                            )
                                        ),
                                        Text("Van már fiókod?",style: TextStyle(color: WHITE_COLOR),),
                                        Expanded(
                                            child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Divider(
                                                color: Colors.lightBlue,
                                              ),
                                            )
                                        ),
                                      ]
                                  ),
                                  SizedBox(height: pice/5,),
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: ConstrainedBox(
                                      constraints: BoxConstraints(
                                          minWidth: MediaQuery.of(context).size.width/3
                                      ),
                                      child: RoundedButton(
                                          title: 'Bejelentkezés',
                                          color: TENT_COLOR,
                                          onPressed: login),
                                    ),
                                  ),
                                  SizedBox(height: pice/5,),
                                ],
                              ),
                            );
                          }
                          return Container();
                        });
                  }
                  return Container();
                }),


          ),
        ),
        onWillPop: () async{
          await SystemChannels.platform.invokeMethod<void>('SystemNavigator.pop');
          return true;
        },
      ),
    );
  }

  void login(){
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => LoginPage()));
  }

  void register(){
    if(Validators.isValidEmail(_emailController.value.text) && Validators.isValidPassword(_passwordControler.value.text) && _nameContorller.value.text.length >=0 && actDropDwon != null) {
      _authBloc.add(
          SigningUp(
              fireStoreUser: FireStoreUser(
                  name: _nameContorller.value.text,
                  teamId: actDropDwon.id,
                  email: _emailController.value.text),
              password: _passwordControler.value.text
          )
      );
    }
  }

  showErrorSignUp(BuildContext context, String error) {
    Widget okButton = RoundedButton(
      color: TENT_COLOR,
      title: "Ok",
      onPressed: () {
        Navigator.pop(context);
      },
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogPopup(
          title: "Hiba a regisztráláskor!",
          message: error,
          actions: [
            okButton,
          ],
        );
      },
    );
  }
}