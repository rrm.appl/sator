import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:szsakd/bloc/auth/auth_bloc.dart';
import 'package:szsakd/bloc/auth/auth_event.dart';
import 'package:szsakd/bloc/auth/auth_state.dart';
import 'package:szsakd/components/auth/register_page.dart';
import 'package:szsakd/components/main/main_map_page.dart';

class AuthNav extends StatefulWidget{
  @override
  _AuthNav createState() => _AuthNav();
}

class _AuthNav extends State<AuthNav>{

  AuthBloc _authBloc;
  @override
  void initState() {
    _authBloc = BlocProvider.of<AuthBloc>(context);
    _authBloc.add(StartingApp());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: BlocConsumer<AuthBloc, AuthState>(
          listener: (context, state){
            if(state is Authenticated){
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => MainMapPage(fireStoreUser: state.fireStoreUser)));
            }
            if(state is Unauthenticated){
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => RegisterPage()));
            }
          },
          builder: (context, state){
            if(state is LoadingAuth){
              return Center(child: CircularProgressIndicator());
            }
            return Container();
          }),
    );
  }

}