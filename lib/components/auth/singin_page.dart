import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:szsakd/bloc/auth/auth_bloc.dart';
import 'package:szsakd/bloc/auth/auth_event.dart';
import 'package:szsakd/bloc/auth/auth_state.dart';
import 'package:szsakd/components/auth/register_page.dart';
import 'package:szsakd/components/common/rounded_button.dart';
import 'package:szsakd/components/common/dialog_pop_up.dart';
import 'package:szsakd/components/main/main_map_page.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/utils/colors.dart';
import 'package:szsakd/utils/validators.dart';

class LoginPage extends StatefulWidget{
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>{

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordControler = TextEditingController();
  AuthBloc _authBloc;
  bool visiblePassword;
  @override
  void initState() {
    _authBloc = BlocProvider.of<AuthBloc>(context);
    visiblePassword = true;
  }

  @override
  Widget build(BuildContext context) {
    final pice = MediaQuery.of(context).size.height/10;
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: BlocConsumer<AuthBloc, AuthState>(
            listener: (context, state){
              if(state is Authenticated){
                debugPrint("auth???");
                Config.currentUser = state.fireStoreUser;
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => MainMapPage(fireStoreUser: state.fireStoreUser)));
              }
              if(state is NotLoggedIn){
                debugPrint("auth!!!?");
                if(state.error != null) {
                  showErrorLogIn(context, state.error);
                }
              }
            },
            builder: (context, state){
              if(state is LoadingAuth){
                return Center(child:  Align(alignment: Alignment.center,child: CircularProgressIndicator()));
              }
              if(state is Unauthenticated || state is NotLoggedIn || state is NotSignedUp){
                return
                  SingleChildScrollView(
                    reverse: true,
                    child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(32.0),
                        child: Center(
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                  maxHeight: MediaQuery.of(context).size.height/9
                              ),
                              child: Image(
                                  image: AssetImage('assets/logo.png')
                              ),
                            )
                        ),
                      ),
                      Align(
                          alignment: Alignment.center,
                          child: Text( 'Bejelentkezés',style: TextStyle(color: WHITE_COLOR),textScaleFactor: 1.5,)
                      ),
                      SizedBox(height: pice/2,),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: _emailController,
                          validator: (value){
                            if(Validators.isValidEmail(value)){
                              return null;
                            }else{
                              return 'Helytelen e-mail cim formátum.';
                            }
                          },
                          style: TextStyle(color: WHITE_COLOR),
                          textAlign: TextAlign.left,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'E-mail...',
                            hintStyle: TextStyle(color: WHITER_COLOR),
                            contentPadding: EdgeInsets.all(16),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: WHITER_COLOR),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: WHITE_COLOR),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: _passwordControler,
                          validator: (value){
                            if(Validators.isValidPassword(value)){
                              return null;
                            }else{
                              return 'Minimum 8 hosszu, 1 kis betü, 1 nagy betü, 1 szám.';
                            }
                          },
                          autovalidateMode: AutovalidateMode.always,
                          obscureText: visiblePassword,
                          style: TextStyle(color: WHITE_COLOR),
                          textAlign: TextAlign.left,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Jelszó...',
                            hintStyle: TextStyle(color: WHITER_COLOR),
                            contentPadding: EdgeInsets.all(16),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: WHITER_COLOR),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: WHITE_COLOR),
                            ),
                            suffixIcon: GestureDetector(
                                child: Icon(Icons.visibility,color: WHITE_COLOR),
                                onLongPress: (){
                                  setState(() {
                                    visiblePassword = false;
                                  });
                                },
                                onLongPressUp: (){
                                  setState(() {
                                    visiblePassword = true;
                                  });
                                },
                              )
                          ),
                        ),
                      ),
                      SizedBox(height: pice/2,),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                              minWidth: MediaQuery.of(context).size.width/3
                          ),
                          child: RoundedButton(
                              title: 'Bejelentkezés',
                              color: TENT_COLOR,
                              onPressed: login
                          ),
                        ),
                      ),
                      SizedBox(height: pice/2,),
                      Row(
                          children: <Widget>[
                            Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Divider(
                                    color: Colors.lightBlue,
                                  ),
                                )
                            ),
                            Text("Nincs még fiókod?",style: TextStyle(color: WHITE_COLOR),),
                            Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Divider(
                                    color: Colors.lightBlue,
                                  ),
                                )
                            ),
                          ]
                      ),
                      SizedBox(height: pice/5,),
                      Align(
                        alignment: Alignment.topCenter,
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                              minWidth: MediaQuery.of(context).size.width/3
                          ),
                          child: RoundedButton(
                              title: 'Regisztárció',
                              color: TENT_COLOR,
                              onPressed: register),
                        ),
                      ),
                    ],
                ),
                  );
              }
              return Container();
            }),
      ),
    );
  }

  void login(){
    if(Validators.isValidEmail(_emailController.value.text) && Validators.isValidPassword(_passwordControler.value.text)) {
      debugPrint("login");
      _authBloc.add(LoggingIn(email: _emailController.value.text,
          password: _passwordControler.value.text));
    }
  }

  void register(){
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => RegisterPage()));
  }

  showErrorLogIn(BuildContext context, String error) {
    Widget okButton = RoundedButton(
      color: TENT_COLOR,
      title: "Ok",
      onPressed: () {
        Navigator.pop(context);
      },
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogPopup(
          title: "Hiba a bejelentkezéssel!",
          message: error,
          actions: [
            okButton,
          ],
        );
      },
    );
  }
}