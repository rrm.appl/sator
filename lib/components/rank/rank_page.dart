import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:szsakd/bloc/rank/rank_bloc.dart';
import 'package:szsakd/bloc/rank/rank_event.dart';
import 'package:szsakd/bloc/rank/rank_state.dart';
import 'package:szsakd/model/team_rank_model.dart';
import 'package:szsakd/utils/colors.dart';
import 'package:szsakd/utils/widget_settings.dart';


class RankPage extends StatefulWidget{
  @override
  _RankPage createState() => _RankPage();
}

class _RankPage extends State<RankPage>{

  bool _filterDec;
  List<TeamRank> teamRanks;
  List<TeamRank> _filtered;
  TextEditingController _teamController = TextEditingController();
  @override
  void initState() {
    teamRanks = [];
    BlocProvider.of<RankBloc>(context)..add(GetTeamRanks());
    _filtered = [];
    _teamController..text='';
    _filterDec=true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: PRIMARY_COLOR,
      body: SafeArea(
        child: Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            decoration: BoxDecoration(
                color: PRIMARY_COLOR,
                borderRadius: BorderRadius.all(Radius.circular(ROUNDED_PADDING))
            ),
            height: MediaQuery.of(context).size.height,
            child:  BlocConsumer<RankBloc, RankState>(
                listener: (context, state){
                  if(state is TeamRanksLoadedState){
                    if(state.teamRanks != null){
                      setState(() {
                        teamRanks = state.teamRanks;
                        filter();
                      });
                    }
                  }
                },
                builder: (context, state){
                  if(state is TeamRanksLoadedState){
                    return  Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: TextFormField(
                                    controller: _teamController,
                                    onChanged: (_){
                                      setState(() {
                                        filter();
                                      });
                                    },
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.all(8.0),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(ROUNDED_PADDING),
                                        borderSide: BorderSide(
                                            color: WHITE_COLOR
                                        ),
                                      ),
                                      focusedBorder:  OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(ROUNDED_PADDING),
                                        borderSide: BorderSide(
                                            color: WHITE_COLOR
                                        ),
                                      ),
                                      enabledBorder:  OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(ROUNDED_PADDING),
                                        borderSide: BorderSide(
                                            color: WHITE_COLOR
                                        ),
                                      ),
                                      hintText: "Csapatok...",
                                      fillColor: WHITE_COLOR,
                                      filled: true,
                                      prefixIcon: Padding(
                                        padding: EdgeInsets.all(2.0), // add padding to adjust icon
                                        child: Icon(Icons.search),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            IconButton(
                              icon: _filterDec ? Icon(Icons.arrow_upward, color: WHITE_COLOR,) : Icon(Icons.arrow_downward, color: WHITE_COLOR,) ,
                              onPressed: (){
                                setState(() {
                                  _filtered = _filtered.reversed.toList();
                                  _filterDec = !_filterDec;
                                });
                              })
                          ],
                        ),
                        Flexible(
                          child: ListView.builder(
                            itemCount: _filtered.length,
                            itemBuilder: (_, i) => Container(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(ROUNDED_PADDING),
                                        border: Border.all(
                                            width: 2.0
                                        ),
                                        color: WHITE_COLOR
                                    ),
                                    child: ListTile(
                                        title: Row(
                                            children: [
                                              Expanded(
                                                  flex: 2,
                                                  child: Text('${_filtered[i].rank}.',textScaleFactor: 1.1)
                                              ),
                                              Expanded(
                                                  flex: 6,
                                                  child: Text('${_filtered[i].name}',textScaleFactor: 1.1, overflow: TextOverflow.ellipsis)
                                              ),
                                              Expanded(
                                                  flex: 2,
                                                  child: Align(
                                                      alignment: Alignment.center,
                                                      child: Text('${_filtered[i].point} pont'))
                                              ),
                                            ]
                                        ),
                                        subtitle: Text('${_filtered[i].allDistance} km')
                                    ),
                                  ),
                                )
                            ),
                          ),
                        ),
                      ],
                    );
                  }
                  if(state is RankStateLoadingState){
                    return Center(
                        child: CircularProgressIndicator()
                    );
                  }
                  return Container();
                }),
            ),
          ),
        ),
      ),
    );
  }

  void filter(){
    final filter = teamRanks.where((e) => e.name.toLowerCase().contains(_teamController.value.text.toLowerCase())).toList();
    setState(() {
      _filtered = filter;
      _filtered.sort((b,a) => a.point.compareTo(b.point));
    });
  }
}