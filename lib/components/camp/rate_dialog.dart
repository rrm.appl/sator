import 'package:flutter/material.dart';
import 'package:szsakd/repos/rank/rank_service.dart';
import 'package:szsakd/utils/widget_settings.dart';

import 'package:flutter/widgets.dart';
import 'package:szsakd/components/common/rounded_button.dart';
import 'package:szsakd/utils/colors.dart';

class DialogWithContent extends StatefulWidget{
  String uid;
  DialogWithContent({this.uid});
  @override
  _DialogWithContent createState() => _DialogWithContent();
}

class _DialogWithContent extends State<DialogWithContent>{
  List<bool> ratingStarts;
  int score;
  @override
  void initState() {
    score = 0;
    ratingStarts = [false,false,false,false,false];
  }

  @override
  Widget build(BuildContext context) {
    Widget okButton = RoundedButton(
      color: TENT_COLOR,
      title: "Értékelés",
      onPressed: () {
        RankService.updateRating(widget.uid,score);
        Navigator.pop(context);
        Navigator.pop(context);
      },
    );
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(ROUNDED_PADDING))
      ),
      title: Text('Értékelje a táborhelyet'),
      content: Row(
        children: [
          IconButton(
            icon: Icon(ratingStarts[0] ? Icons.star : Icons.star_rate_outlined),
            onPressed: (){
              setTrue(0);
            },
          ),
          IconButton(
            icon: Icon(ratingStarts[1] ? Icons.star : Icons.star_rate_outlined),
            onPressed: (){
              setTrue(1);
            },
          ),
          IconButton(
            icon: Icon(ratingStarts[2] ? Icons.star : Icons.star_rate_outlined),
            onPressed: (){
              setTrue(2);
            },
          ),
          IconButton(
            icon: Icon(ratingStarts[3] ? Icons.star : Icons.star_rate_outlined),
            onPressed: (){
              setTrue(3);
            },
          ),
          IconButton(
            icon: Icon(ratingStarts[4] ? Icons.star : Icons.star_rate_outlined),
            onPressed: (){
              setTrue(4);
            },
          ),
        ],
      ),
      actions: [okButton],
    );
  }

  void setTrue(int inp){
    setState(() {
      for(int i=0; i<=4; i++){
        if(i<=inp){
          ratingStarts[i] = true;
          score = inp+1;
        }else{
          ratingStarts[i] = false;
        }
      }
    });
  }
}