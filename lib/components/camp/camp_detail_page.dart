import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:szsakd/components/camp/information_page.dart';
import 'package:szsakd/components/common/rounded_button.dart';
import 'package:szsakd/components/camp/rate_dialog.dart';
import 'package:szsakd/model/camp_model.dart';
import 'package:szsakd/utils/colors.dart';

class CampDetailPage extends StatefulWidget{
  Camp camp;
  CampDetailPage({this.camp});
  @override
  _CampDetailPage createState() => _CampDetailPage();
}

class _CampDetailPage extends State<CampDetailPage>{

  List<String> items;
  List<bool> starts;
  List<bool> ratingStarts;

  @override
  void initState() {
    ratingStarts = [false,false,false,false,false];
    starts = [];
    for(int i=1; i<=5; i++){
      if(i<=widget.camp.rate.score){
        starts.add(true);
      }else{
        starts.add(false);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: PRIMARY_COLOR,
      floatingActionButton: FloatingActionButton(
        heroTag: "campdeatailPage",
        child: Icon(Icons.star_rate_outlined),
        onPressed: (){
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return DialogWithContent(uid: widget.camp.uid);
            },
          );
        },
      ),
      body:  SafeArea(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8.0,bottom: 20.0,top:20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(widget.camp.campName, textScaleFactor: 2,style: TextStyle(color: WHITE_COLOR),),
                    Row(
                      children: [
                        Text('${widget.camp.rate.score}',style: TextStyle(color: WHITE_COLOR),),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Row(
                            children: starts.map((e){
                                return Icon(Icons.star,color: e ? Colors.orangeAccent : Colors.grey);
                              }).toList()
                          ),
                        ),
                        Text("(${widget.camp.rate.rated})",style: TextStyle(color: WHITE_COLOR),)
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 8,
                child: InformationPage(camp: widget.camp),
              )
            ],
          )
      ),

    );
  }

  showCloudUpdateAlertDialog(BuildContext context) {
    Widget okButton = RoundedButton(
      color: TENT_COLOR,
      title: "Értékelés",
      onPressed: () {
        Navigator.pop(context);
        Navigator.pop(context);
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Text('Értékelje a táborhelyet'),
            content: Row(
              children: [
                IconButton(
                  icon: Icon(ratingStarts[0] ? Icons.star : Icons.star_rate_outlined),
                  onPressed: (){
                    setState(() {
                      for(int i=0; i<=4; i++){
                        if(i==0){
                          ratingStarts[i] = true;
                        }else{
                          ratingStarts[i] = true;
                        }
                      }
                    });
                  },
                ),
                IconButton(
                  icon: Icon(ratingStarts[1] ? Icons.star : Icons.star_rate_outlined),
                  onPressed: (){
                    setState(() {
                      for(int i=0; i<=4; i++){
                        if(i<=1){
                          ratingStarts[i] = true;
                        }else{
                          ratingStarts[i] = true;
                        }
                      }
                    });
                  },
                ),
                IconButton(
                  icon: Icon(ratingStarts[2] ? Icons.star : Icons.star_rate_outlined),
                  onPressed: (){
                    for(int i=0; i<=4; i++){
                      if(i<=2){
                        ratingStarts[i] = true;
                      }else{
                        ratingStarts[i] = true;
                      }
                    }
                  },
                ),
                IconButton(
                  icon: Icon(ratingStarts[3] ? Icons.star : Icons.star_rate_outlined),
                  onPressed: (){
                    for(int i=0; i<=4; i++){
                      if(i<=3){
                        ratingStarts[i] = true;
                      }else{
                        ratingStarts[i] = true;
                      }
                    }
                  },
                ),
                IconButton(
                  icon: Icon(ratingStarts[4] ? Icons.star : Icons.star_rate_outlined),
                  onPressed: (){
                    setState(() {

                    });
                    for(int i=0; i<=4; i++){
                      if(i<=4){
                        ratingStarts[i] = true;
                      }else{
                        ratingStarts[i] = false;
                      }
                    }
                  },
                ),
              ],
            ),
            actions: [okButton],
        );
      },
    );
  }

}