import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:szsakd/model/camp_model.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

class InformationPage extends StatelessWidget{
  Camp camp;
  InformationPage({this.camp});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
          children: [
            OneInfoDetail(icon: Icons.local_drink, text: camp.drinkWater),
            OneInfoDetail(icon: Icons.pool, text: camp.bathWater),
            OneInfoDetail(icon: Icons.shopping_cart, text: camp.shop),
            OneInfoDetail(icon: Icons.sports_volleyball, text: camp.playground),
            OneInfoDetail(icon: Icons.attach_money, text: camp.price),
            Card(
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.white70, width: 1),
                borderRadius: BorderRadius.circular(10),
              ),
              child: ListTile(
                leading: Icon(Icons.phone),
                title: Text('${camp.contact}'),
                onTap:() {
                  if(camp.contact != null){
                    UrlLauncher.launch("tel:${camp.contact}");
                  }
                },
              ),
            )
          ],
        ),
    );
  }
}


class OneInfoDetail extends StatelessWidget{
  IconData icon;
  String text;
  Function onPress;
  OneInfoDetail({this.icon,this.text,this.onPress});
  @override
  Widget build(BuildContext context) {
      return Card(
          shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.white70, width: 1),
            borderRadius: BorderRadius.circular(10),
          ),
          child: ListTile(
            leading: Icon(icon),
            title: Text(text),
            onTap: onPress==null ? null : () => onPress(),
          ),
        );
  }
}