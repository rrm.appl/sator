import 'dart:async';
import 'dart:math';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';
import 'package:szsakd/bloc/auth/auth_bloc.dart';
import 'package:szsakd/bloc/auth/auth_event.dart';
import 'package:szsakd/bloc/auth/auth_state.dart';
import 'package:szsakd/bloc/camp/camp_bloc.dart';
import 'package:szsakd/bloc/camp/camp_event.dart';
import 'package:szsakd/bloc/camp/camp_state.dart';
import 'package:szsakd/bloc/hiking/hiking_bloc.dart';
import 'package:szsakd/bloc/hiking/hiking_event.dart';
import 'package:szsakd/bloc/hiking/hiking_state.dart';
import 'package:szsakd/components/auth/register_page.dart';
import 'package:szsakd/components/camp/camp_detail_page.dart';
import 'package:szsakd/components/common/rounded_button.dart';
import 'package:szsakd/components/common/dialog_pop_up.dart';
import 'package:szsakd/components/offlinemap/read_from_db.dart';
import 'package:szsakd/components/rank/rank_page.dart';
import 'package:szsakd/components/tracking/tracker_page.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/model/camp_marker_model.dart';
import 'package:szsakd/model/camp_model.dart';
import 'package:szsakd/model/rate_mode.dart';
import 'package:szsakd/model/user_model.dart';
import 'package:szsakd/repos/rank/rank_service.dart';
import 'package:szsakd/utils/download_offline_map.dart';
import 'package:szsakd/utils/map_settings.dart';
import 'package:szsakd/utils/colors.dart';
import 'package:szsakd/utils/download_map.dart';
import 'package:szsakd/utils/widget_settings.dart';

class MainMapPage extends StatefulWidget {
  final FireStoreUser fireStoreUser;
  const MainMapPage({Key key, this.fireStoreUser}) : super(key: key);
  @override
  _MainMapPageState createState() => _MainMapPageState();
}

class _MainMapPageState extends State<MainMapPage> with TickerProviderStateMixin{

  LatLng _current;

  MapController _mapController;

  bool _isMapReady;
  final _scaffoldKey = GlobalKey<ScaffoldState>();


  bool _isShowAddPanel; // tábor vagy kirándulás hozzáadás panel
  bool _isShowCamp; // egy táborhely kiválasztása a térképről
  bool _isShowNewCamp; // új táborhely hozzáadás panel

  List<Marker> _campsList;
  List<CampMarker> _savedCampInfromation;
  Camp _currentCamp;

  List<CampMarker> _campInformationMarkers; //táborról információk
  LatLng _centerMarker; //center poit a markerkhez
  bool _isSettingMarker;
  int index;
  bool afterLayout;
  bool _notSavedNewCamp; //van e még nem mentett ezt valahonnan a hivéből is kikéne szedni
  bool _isShowInformationMarkers;
  TextEditingController _campNameController = TextEditingController();
  TextEditingController _drinkWaterController = TextEditingController();
  TextEditingController _bathWaterController = TextEditingController();
  TextEditingController _shopController = TextEditingController();
  TextEditingController _playGroundController = TextEditingController();
  TextEditingController _priceController = TextEditingController();
  TextEditingController _contactController = TextEditingController();

  StreamSubscription<DataConnectionStatus> listener;
  bool _isConnection;
  List<int> _herotags;

  CampBloc _campBloc;
  bool _isNotSavedHiking;

  bool downloadingMap;
  bool isdownloadedMapUse;
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_){
      setState(() {
        afterLayout = true;
      });
    });
    super.initState();
    afterLayout = false;

    _isShowAddPanel = false;
    _isShowCamp = false;
    _isShowNewCamp = false;

    _mapController = MapController();
    _mapController.onReady.then((value){
      _isMapReady = _mapController.ready;
    });
    _positionCurrent();

    //táborhely infármcióinak kijelölésére
    initNewCampSetting();

    _herotags = [];

    _isConnection = false;
    checkConnection();

    _campBloc = BlocProvider.of<CampBloc>(context);
    _campBloc.add(GetCampLocal());
    _campBloc.add(GetCampCloud());
    _isNotSavedHiking = false;
    HikingBloc _hikingLocalBloc = BlocProvider.of<HikingBloc>(context);
    _hikingLocalBloc.add(GetHikingLocal());

    _campsList = [];
    _savedCampInfromation = [
      CampMarker(color: BLUE_COLOR),
      CampMarker(color: ACCENTBLUE_COLOR),
      CampMarker(color: GREEN_COLOR),
      CampMarker(color: ORANGE_COLOR),
    ];
    _currentCamp = null;
    _campsList = [];

    downloadingMap = false;
    isdownloadedMapUse = false;
  }

  @override
  void dispose() {
    listener.cancel();
    super.dispose();
  }

  void initNewCampSetting(){
    _campInformationMarkers = [
      CampMarker(color: BLACK_COLOR),
      CampMarker(color: BLUE_COLOR),
      CampMarker(color: ACCENTBLUE_COLOR),
      CampMarker(color: GREEN_COLOR),
      CampMarker(color: ORANGE_COLOR),
    ];
    _centerMarker = null;
    _isSettingMarker = false;
    _notSavedNewCamp = false;
    _isShowInformationMarkers = false;
    _campNameController..text = '';
    _drinkWaterController..text = '';
    _bathWaterController..text = '';
     _shopController..text = '';
     _playGroundController..text = '';
    _priceController..text = '';
    _contactController..text = '';
  }

  checkConnection() async {
    listener = DataConnectionChecker().onStatusChange.listen((status) {
      switch (status) {
        case DataConnectionStatus.connected:
          final snackBar = SnackBar(content: Text('Van internet kapcsolat'), backgroundColor: GREEN_COLOR);
          _scaffoldKey.currentState.showSnackBar(snackBar);
          setState(() {
            _isConnection = true;
          });
          break;
        case DataConnectionStatus.disconnected:
          final snackBar = SnackBar(content: Text('Nincs internet kapcsolat'),backgroundColor: RED_COLOR,);
          _scaffoldKey.currentState.showSnackBar(snackBar);
          setState(() {
            _isConnection = false;
          });
          break;
      }
    });
    return await DataConnectionChecker().connectionStatus;
  }

  //center markerhez
  void setOnPositionChanged(LatLng position){
    if(afterLayout){
      setState(() {
        _centerMarker = position;
      });
    }
  }

  //aktuális pozició
  Future<void> _positionCurrent() async{
    await Geolocator.getCurrentPosition().then((value){
      setState(() {
        _current = LatLng(value.latitude, value.longitude);
        _moveCameraCenter(_current,15);
      });
    });
  }

  //randomize herotag
  String getHeroTag(){
    var rng = new Random();
    int random = rng.nextInt(100000);
    while(_herotags.contains(random)){
      random = rng.nextInt(1000);
    }
    _herotags.add(random);
    return random.toString();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        key: _scaffoldKey,
        endDrawer: Drawer(
            child: ListView(
              children: <Widget>[
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text('${widget.fireStoreUser.name}'),
                ),
                FutureBuilder(
                  future: RankService.getTeamName(widget.fireStoreUser.teamId),
                  builder: (BuildContext context, snapshot) {
                    if(snapshot.hasData){
                      if(snapshot.connectionState == ConnectionState.waiting){
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }else{
                        return Center( // here only return is missing
                            child: ListTile(leading: Icon(Icons.people), title: Text('${snapshot.data}')),
                        );
                      }
                    }else if (snapshot.hasError){
                      Text('no data');
                    }
                    return CircularProgressIndicator();
                  },
                ),
                Divider(),
                ListTile(leading: Icon(Icons.list_alt), title: Text("Helyezés"),
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => RankPage()));
                  },
                ),
                Divider(),
                FutureBuilder(
                  future: DownloadOfflineMap.existsMap("maptiles[1].db"),
                  builder: (BuildContext context, snapshot) {
                    if(snapshot.hasData){
                      if(snapshot.connectionState == ConnectionState.waiting){
                        return Center(
                          child: Container(),
                        );
                      }else{
                        return ListTile(
                          leading: Icon(Icons.map),
                          title:  !downloadingMap && snapshot.data ?
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Magyarország"),
                              Switch(
                                  value: isdownloadedMapUse,
                                  onChanged: (value){
                                      setState(() {
                                        isdownloadedMapUse = !isdownloadedMapUse;
                                      });
                                  }),
                            ],
                          ) :
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Magyarország"),
                              downloadingMap ?
                              CircularProgressIndicator() :
                              IconButton(
                                icon: Icon(Icons.download_sharp),
                                onPressed: () async {
                                  setState(() {
                                    downloadingMap = true;
                                  });
                                  await DownloadOfflineMap.downloadURLExample('maptiles[1].db');
                                  setState(() {
                                    downloadingMap = false;
                                    isdownloadedMapUse = true;
                                  });
                                  Config.notification.showNotification("Sikeres letöltés", "Magyarország térkép letöltése sikeresen befejeződött.");
                                  debugPrint("letoltotte");
                                },
                              )
                            ],
                          ),
                        );
                      }
                    }else if (snapshot.hasError){
                      Text('no data');
                    }
                    return CircularProgressIndicator();
                  },
                ),
                Divider(),
                BlocConsumer<AuthBloc, AuthState>(
                    listener: (context, state){
                      if(state is UserDataChange){
                          setState(() {
                          });
                      }
                      if(state is Unauthenticated){
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => RegisterPage(),
                          ),
                              (route) => false,
                        );
                      }
                    },
                    builder: (context, state){
                      if(state is Authenticated ){
                        return  ListTile(
                            leading: Icon(Icons.logout),
                            title: Text("Kijelentkezés"),
                            onTap: (){
                              BlocProvider.of<AuthBloc>(context)..add(LoggingOut());
                            });
                      }
                      return Container();
                    }),
              ],
            )),
        resizeToAvoidBottomInset: false,
          body: SafeArea(
            child: Stack(
              children: [
                FlutterMap(
                  mapController: _mapController,
                  options: MapOptions(
                    center: _current != null ? _current : LatLng(47.49801, 19.03991),
                    zoom: 8.0,
                    maxZoom: 17.0,
                    minZoom: 3,
                    plugins: [
                      MarkerClusterPlugin()
                    ],
                    onPositionChanged: (position,bool) {
                      setOnPositionChanged(position.center);
                    },
                  ),
                  layers: [
                    !isdownloadedMapUse ? TileLayerOptions(
                        tileProvider: NonCachingNetworkTileProvider(),
                        urlTemplate: "https://tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png",
                        subdomains: ['a','b','c']
                    ) :
                    TileLayerOptions(
                        tileProvider: DbTile.fromFile('maptiles[1].db')
                    ),
                    //current location
                    MarkerLayerOptions(
                      markers: _current != null ? [
                        Marker(
                            point: _current,
                            width: 30,
                            height: 30,
                            builder: (ctx) =>
                                IconButton(
                                  icon: Icon(Icons.circle,color: ACCENTBLUE_COLOR,),
                                )
                        ),
                      ] : [],
                    ),
                    //egyes infomrációk markerei új táborhelynél
                    MarkerLayerOptions(
                      markers: _isShowInformationMarkers ? _getCampInformationMarkers(_campInformationMarkers) : []
                    ),
                    //egyes infomrációk markerei egy adott táborhelynél
                    MarkerLayerOptions(
                        markers: _isShowCamp ? _getCampInformationMarkers(_savedCampInfromation) : []
                    ),
                    //kijelölés
                    MarkerLayerOptions(
                        markers:  _isSettingMarker ? [
                          Marker(
                              point: _centerMarker,
                              width: 30,
                              height: 30,
                              builder: (ctx) =>
                                  IconButton(
                                    icon: Icon(Icons.location_on,color: _campInformationMarkers[index].color,),
                                    onPressed: (){
                                      setState(() {
                                        _campInformationMarkers[index].position = _centerMarker;
                                        _isSettingMarker = false;
                                        index = -1;
                                      });
                                    },
                                  )
                          )
                        ] : []
                    ),
                    MarkerClusterLayerOptions(
                      maxClusterRadius: 120,
                      size: Size(40, 40),
                      fitBoundsOptions: FitBoundsOptions(
                        padding: EdgeInsets.all(MediaQuery.of(context).size.width/3),
                      ),
                      markers: _isShowInformationMarkers ? [] : _campsList,
                      polygonOptions: PolygonOptions(
                          borderColor: ACCENTBLUE_COLOR,
                          color: BLACK12_COLOR,
                          borderStrokeWidth: 3),
                      builder: (context, markers) {
                        return FloatingActionButton(
                          heroTag: getHeroTag(),
                          child: Text(markers.length.toString()),
                          onPressed: null,
                        );
                      },)
                  ],
                ),
                BlocConsumer<CampBloc, CampState>(
                    listener: (context, state){
                      if(state is CampCloudLoadedState) {
                          if(state.camps != null){
                            List<Marker> markers = [];
                            if(_currentCamp != null){
                              _currentCamp = state.camps.where((e) => e.uid == _currentCamp.uid).first;
                            }
                            state.camps.forEach((curr) {
                              final marker = Marker(
                                  width: 30,
                                  height: 30,
                                  point: LatLng(curr.campName_lat, curr.campName_lon), // bal- fent
                                  builder: (ctx) =>
                                      IconButton(
                                        icon: Icon(Icons.location_on),
                                        onPressed: () async {
                                          setState(() {
                                            _savedCampInfromation[0].position = curr.drinkWater_lat != null ? LatLng(curr.drinkWater_lat, curr.drinkWater_lon) : null;
                                            _savedCampInfromation[1].position = curr.bathWater_lat != null ? LatLng(curr.bathWater_lat, curr.bathWater_lon) : null;
                                            _savedCampInfromation[2].position = curr.shop_lat != null ? LatLng(curr.shop_lat, curr.shop_lon) : null;
                                            _savedCampInfromation[3].position = curr.playground_lat != null ? LatLng(curr.playground_lat, curr.playground_lon) : null;
                                            _currentCamp=curr;
                                            _isShowCamp = true;
                                            MapSetting.moveCameraCenter(LatLng(curr.campName_lat, curr.campName_lon), 12, this,_mapController);
                                          });
                                        },
                                      ));
                              markers.add(marker);
                            });
                            setState(() {
                              _campsList = markers;
                            });
                          }
                      }
                    },
                    builder: (context, state){return Container();}),
                _isShowCamp ? Container() : Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: WHITE_COLOR,
                          borderRadius: BorderRadius.all(Radius.circular(ROUNDED_PADDING))
                      ),
                      height: 50,
                      child: IconButton(icon: Icon(Icons.person, color: BLACK_COLOR,), onPressed: (){
                        _scaffoldKey.currentState.openEndDrawer();
                      }),
                    ),
                  ),
                ),//táborhely kereső
                _isShowAddPanel ? _getAddPanel() : Container(), //új táborhely vagy kirándulás panel
                _isShowCamp ? _getCampPanel() : Container(), // egy konkrét táborhely
                _isShowNewCamp ? _getNewCampPanel() : Container() //újtáborhely panel
              ],
            ),
          ),
          floatingActionButton: (_isShowAddPanel || _isShowCamp || _isShowNewCamp) ? null : Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
            Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FloatingActionButton(
                  heroTag: 'center',
                    backgroundColor: WHITE_COLOR,
                    child: Icon(Icons.my_location,color: TENT_COLOR,),
                    onPressed: () async{
                      _positionCurrent();
                      //await TileDownload.download(12);
                    },),
                ),
              ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _isNotSavedHiking ? Padding(
                  padding: const EdgeInsets.only(left: 25.0,right:8.0,bottom:8.0,top: 8.0),
                  child: FloatingActionButton(
                    heroTag: 'notsavedHiking',
                    backgroundColor: PRIMARY_COLOR,
                    child: Icon(Icons.directions_walk,color: WHITE_COLOR,),
                    onPressed: () async {
                      final _pop = await Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) =>
                              TrackerPage(isRecordedShow: false,)));
                      setState(() {
                        _isNotSavedHiking = _pop;
                        if (_pop == true) {
                          HikingBloc _hikingLocalBloc = BlocProvider.of<HikingBloc>(context);
                          _hikingLocalBloc.add(GetHikingLocal());
                        }
                      });
                    }),
                ): BlocConsumer<HikingBloc, HikingState>(
                    listener: (context, state){
                      if(state is HikingLocalLoadedState) {
                        if (state.hiking != null) {
                          setState(() {
                            _isNotSavedHiking = true;
                          });
                        }
                      }
                    },
                    builder: (context, state){return Container();}),
                Align(
                  alignment: Alignment.centerRight,
                    child:  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FloatingActionButton(
                      heroTag: 'startRecord',
                      backgroundColor: TENT_COLOR,
                      child: Icon(Icons.directions_walk),
                      onPressed: () async{
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => TrackerPage(isRecordedShow: true,)));
                      },
                    ),
                  )),
              ],
            ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _notSavedNewCamp  ? Padding(
              padding: const EdgeInsets.only(left: 25.0,right:8.0,bottom:8.0,top: 8.0),
        child: FloatingActionButton(
          heroTag: 'notsavedCamp',
          backgroundColor: PRIMARY_COLOR,
          child: ImageIcon(AssetImage('assets/camp_icon.png'),color: WHITE_COLOR,),
          onPressed: (){
            setState(() {
              _isShowNewCamp = true;
              _isSettingMarker = false;
              _isShowInformationMarkers = false;
            });
          },),
      ) : BlocConsumer<CampBloc, CampState>(
                      listener: (context, state){
                        if(state is CampLocalLoadedState) {
                          if (state.camp != null) {
                            setState(() {
                              _notSavedNewCamp = true;
                              _campNameController..text = state.camp.campName;
                              _campInformationMarkers[0].position = state.camp.campName_lat != null ? LatLng(state.camp.campName_lat,  state.camp.campName_lon) : null;
                              _drinkWaterController..text = state.camp.drinkWater;
                              _campInformationMarkers[1].position = state.camp.drinkWater_lat != null ? LatLng(state.camp.drinkWater_lat, state.camp.drinkWater_lon): null;
                               _bathWaterController..text = state.camp.bathWater;
                              _campInformationMarkers[2].position = state.camp.bathWater_lat != null ? LatLng(state.camp.bathWater_lat, state.camp.bathWater_lon): null;
                               _shopController..text = state.camp.shop;
                              _campInformationMarkers[3].position = state.camp.shop_lat != null ? LatLng(state.camp.shop_lat, state.camp.shop_lon): null;
                               _playGroundController..text = state.camp.playground;
                              _campInformationMarkers[4].position = state.camp.playground_lat != null ? LatLng(state.camp.playground_lat, state.camp.playground_lon): null;
                               _priceController..text = state.camp.price;
                               _contactController..text = state.camp.contact;
                            });
                          }
                        }
                      },
                      builder: (context, state){return Container();}),
                  (Config.currentUser.edit != null && Config.currentUser.edit) ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FloatingActionButton(
                      heroTag: 'delete',
                      backgroundColor: TENT_COLOR,
                      child: Icon(Icons.add),
                      onPressed: (){
                        //clearRecord();
                        setState(() {
                          _isShowAddPanel = true;
                        });
                      },
                    ),
                  ) : Container(),
                ],
              ),
            ],
          ),
      ),
      onWillPop: () async{
        if(_isShowAddPanel){ //új tábor vagy kirándulás panel eltántetése
          setState(() {
            _isShowAddPanel = false;
          });
          return false;
        }
        if(_isShowNewCamp){ //uj taborhely eltuntetese
          setState(() {
            _isShowNewCamp = false;
            _isShowInformationMarkers = false;
          });
          return false;
        }
        if(_isShowInformationMarkers){
          setState(() {
            _isSettingMarker = false;
            _isShowInformationMarkers = false;
            _isShowNewCamp = true;
          });
          return false;
        }
        if(_isShowCamp){ //adott taborhely eltuntetése
          setState(() {
            _isShowCamp = false;
          });
          return false;
        }
        return true;
      },
    );
  }

  //ha egy konkrétat kivaálasztok akkor ez lehetne az
  List<Marker> _getCampInformationMarkers(List<CampMarker> campInfos){
    List<Marker> _markers = [];
    campInfos.forEach((element) {
      if(element.position != null){
        _markers.add(
            Marker(
                point: element.position,
                width: 30,
                height: 30,
                builder: (ctx) =>
                    IconButton(
                      icon: Icon(Icons.location_on,color: element.color,),
                    )
            )
        );
      }
    });
    return _markers;
  }

  showNotSavedCampAlertDialog(BuildContext context) {
    Widget okButton = RoundedButton(
      color: TENT_COLOR,
      title: "Ok",
      onPressed: () {
        Navigator.pop(context);
        setState(() {
          _isShowAddPanel = false;
          _isShowNewCamp = true;
        });
      },
    );

    Widget exitButton = RoundedButton(
      color: TENT_COLOR,
      title: "Törlés",
      onPressed: () {
        Navigator.pop(context);
        setState(() {
          _notSavedNewCamp = false;
          //törlés minden
        });
      },
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogPopup(
          title: "Van nem feltöltött táborhely!",
          message: "Töltse fel vagy törölje a táborhelyet mielőtt újat felvenne!",
          actions: [
            okButton,
            exitButton
          ],
        );
      },
    );
  }

  showCloudUpdateAlertDialog(BuildContext context) {
    Widget okButton = RoundedButton(
      color: TENT_COLOR,
      title: "ok",
      onPressed: () {
        Navigator.pop(context);
      },
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogPopup(
            title: 'Sikeres feltöltés!',
            message: 'A táborhely sikeresen feltöltésre került.',
            actions: [okButton]);
      },
    );
  }

  Widget _getAddPanel(){
      return Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            decoration: BoxDecoration(
                color: PRIMARY_COLOR,
                borderRadius: BorderRadius.all(Radius.circular(ROUNDED_PADDING))
            ),
            height: MediaQuery
                .of(context)
                .size
                .height / 4,
            child: Align(
              alignment: Alignment.center,
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FloatingActionButton(
                            heroTag: 'tent',
                            backgroundColor: TENT_COLOR,
                            child: ImageIcon(AssetImage('assets/camp_icon.png')),
                            onPressed: () {
                              if(_notSavedNewCamp) {
                                showNotSavedCampAlertDialog(context);
                              }else {
                                setState(() {
                                  _notSavedNewCamp = true;
                                  _isShowNewCamp = true;
                                  _isShowAddPanel = false;
                                });
                              }
                            },
                          ),
                          Text('Tábor', style: TextStyle(color: WHITE_COLOR))
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FloatingActionButton(
                            heroTag: 'walk',
                            backgroundColor: TENT_COLOR,
                            child: Icon(Icons.directions_walk),
                            onPressed: () async {
                              final _pop = await Navigator.of(context).push(MaterialPageRoute(builder: (context) => TrackerPage(isRecordedShow: false,)));
                              setState(() {
                                if(_pop == true){
                                  _isNotSavedHiking = true;
                                  HikingBloc _hikingLocalBloc = BlocProvider.of<HikingBloc>(context);
                                  _hikingLocalBloc.add(GetHikingLocal());
                                }
                                _isShowAddPanel = false;
                              });
                            },
                          ),
                          Text('Kirándulás', style: TextStyle(
                              color: WHITE_COLOR))
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
  }

  Widget _getCampPanel() {
    return Align(
      alignment: Alignment.topCenter,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                FloatingActionButton(
                  heroTag: 'drinkwater',
                  onPressed: (){
                      if(_currentCamp.drinkWater_lat != null){
                        _moveCameraCenter(LatLng(_currentCamp.drinkWater_lat,_currentCamp.drinkWater_lon), 15);
                      }
                  },
                  child: Icon(Icons.local_drink, color: BLUE_COLOR,), backgroundColor: _currentCamp.drinkWater_lat != null ? WHITE_COLOR : GREY_COLOR,),
                FloatingActionButton(
                  heroTag: 'swim',
                  onPressed: (){
                    if(_currentCamp.bathWater_lat != null){
                      _moveCameraCenter(LatLng(_currentCamp.bathWater_lat,_currentCamp.bathWater_lon), 15);
                    }
                  },
                  child: Icon(Icons.pool, color: ACCENTBLUE_COLOR,),backgroundColor: _currentCamp.bathWater_lat != null ? WHITE_COLOR : GREY_COLOR,),
                FloatingActionButton(
                  heroTag: 'cart',
                  onPressed: (){
                    if(_currentCamp.shop_lat != null){
                      _moveCameraCenter(LatLng(_currentCamp.shop_lat,_currentCamp.shop_lon), 15);
                    }
                  },
                  child: Icon(Icons.shopping_cart, color: GREEN_COLOR,),backgroundColor: _currentCamp.shop_lat != null ?  WHITE_COLOR :  GREY_COLOR,),
                FloatingActionButton(
                  heroTag: 'volleyboy',
                  onPressed: (){
                    if(_currentCamp.playground_lat != null){
                      _moveCameraCenter(LatLng(_currentCamp.playground_lat,_currentCamp.playground_lon), 15);
                    }
                  },
                  child: Icon(Icons.sports_volleyball, color: ORANGE_COLOR,),backgroundColor: _currentCamp.playground_lat != null ?  WHITE_COLOR : GREY_COLOR,),
              ],
            ),
          ),
          Expanded(
            flex: 8,
            child: Container(),
          ),
          Expanded(
            flex: 1,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Text(
                        '${_currentCamp.campName}',
                        textScaleFactor: 1.2,
                      ),
                    ),
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children:[
                          IconButton(
                              color: RED_COLOR,
                              icon: Icon(Icons.info,color: GREY_COLOR,),
                              onPressed: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => CampDetailPage(camp: _currentCamp)));
                              }
                          ),
                          Container(
                              color: BLUE_COLOR
                          ),
                          IconButton(
                              icon: Icon(Icons.cancel,color: RED_COLOR,),
                              onPressed: (){
                                setState(() {
                                  _isShowCamp = false;
                                });
                              }
                          ),
                        ]
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _getNewCampPanel(){
    final pice = MediaQuery.of(context).size.height/10;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
            color: PRIMARY_COLOR,
            borderRadius: BorderRadius.all(Radius.circular(ROUNDED_PADDING))
        ),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: BlocConsumer<CampBloc, CampState>(
            listener: (context, state){
              if(state is CampLocalLoadedState) {
                if(state.delete){
                  setState(() {
                    initNewCampSetting();
                    _isShowNewCamp = false;
                  });
                }
              }
              if(state is CampCloudLoadedState){
                setState(() {
                  initNewCampSetting();
                  _isShowNewCamp = false;
                  _campBloc.add(GetCampCloud());
                });
                showCloudUpdateAlertDialog(_scaffoldKey.currentContext);
              }
            },
            builder: (context, state){
              if(state is CampLocalLoadingState || state is  CampCloudLoadingState) {
                return Center(child:  Align(alignment: Alignment.center, child: CircularProgressIndicator()));
              }
              if(state is CampLocalLoadedState || state is CampCloudLoadedState){
                return SingleChildScrollView(
                  reverse: true,
                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(height: pice/2,),
                        Row(
                          children: [
                            IconButton(
                                icon: Icon(Icons.delete,color: WHITE_COLOR),
                                onPressed: (){
                                  setState(() {
                                    _campBloc.add(DeleteCampLocal());
                                  });
                                }
                            ),
                            Expanded(
                              flex: 8,
                              child: Align(
                                  alignment: Alignment.center,
                                  child: Text( 'Vegyél fel új táborhelyet!',style: TextStyle(color: WHITE_COLOR),textScaleFactor: 1.2,)
                              ),
                            ),
                            IconButton(
                                icon: Icon(Icons.map,color: WHITE_COLOR),
                                onPressed: (){
                                  setState(() {
                                    _isShowInformationMarkers = true;
                                    _isShowNewCamp = false;
                                  });
                                }
                            )
                          ],
                        ),
                        SizedBox(height: pice/2,),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  controller: _campNameController,
                                  style: TextStyle(color: WHITE_COLOR),
                                  textAlign: TextAlign.left,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    hintText: 'Tábrohely neve...',
                                    hintStyle: TextStyle(color: WHITER_COLOR),
                                    contentPadding: EdgeInsets.all(16),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITER_COLOR),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITE_COLOR),
                                    ),
                                    prefixIcon: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(Icons.drive_file_rename_outline, color: _campInformationMarkers[0].position != null ? TENT_COLOR : WHITE_COLOR),
                                    ),
                                  ),
                                ),
                              ),
                              IconButton(
                                  color: WHITE_COLOR,
                                  icon: Icon(Icons.my_location),
                                  onPressed: () async{
                                    await Geolocator.getCurrentPosition().then((value){
                                      setState(() {
                                        _campInformationMarkers[0].position = LatLng(value.latitude, value.longitude);
                                      });
                                    });
                                  }
                              ),
                              IconButton(
                                  color: WHITE_COLOR,
                                  icon: Icon(Icons.location_on),
                                  onPressed: (){
                                    setState(() {
                                      index = 0;
                                      _isSettingMarker = true;
                                      _isShowNewCamp = false;
                                      _isShowInformationMarkers = true;
                                    });
                                  }
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  controller: _drinkWaterController,
                                  style: TextStyle(color: WHITE_COLOR),
                                  textAlign: TextAlign.left,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    hintText: 'Ivóviz...',
                                    hintStyle: TextStyle(color: WHITER_COLOR),
                                    contentPadding: EdgeInsets.all(16),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITER_COLOR),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITE_COLOR),
                                    ),
                                    prefixIcon: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(Icons.local_drink, color: _campInformationMarkers[1].position != null ? TENT_COLOR : WHITE_COLOR),
                                    ),
                                  ),

                                ),
                              ),
                              IconButton(
                                  color: WHITE_COLOR,
                                  icon: Icon(Icons.my_location),
                                  onPressed: () async{
                                    await Geolocator.getCurrentPosition().then((value){
                                      setState(() {
                                        _campInformationMarkers[1].position = LatLng(value.latitude, value.longitude);
                                      });
                                    });
                                  }
                              ),
                              IconButton(
                                  color: WHITE_COLOR,
                                  icon: Icon(Icons.location_on),
                                  onPressed: (){
                                    setState(() {
                                      index = 1;
                                      _isSettingMarker = true;
                                      _isShowNewCamp = false;
                                      _isShowInformationMarkers = true;
                                    });
                                  }
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  controller: _bathWaterController,
                                  style: TextStyle(color: WHITE_COLOR),
                                  textAlign: TextAlign.left,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    hintText: 'Fürdés...',
                                    hintStyle: TextStyle(color: WHITER_COLOR),
                                    contentPadding: EdgeInsets.all(16),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITER_COLOR),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITE_COLOR),
                                    ),
                                    prefixIcon: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(Icons.pool, color: _campInformationMarkers[2].position != null ? TENT_COLOR : WHITE_COLOR),
                                    ),
                                  ),

                                ),
                              ),
                              IconButton(
                                  color: WHITE_COLOR,
                                  icon: Icon(Icons.my_location),
                                  onPressed: () async {
                                    await Geolocator.getCurrentPosition().then((value){
                                      setState(() {
                                        _campInformationMarkers[2].position = LatLng(value.latitude, value.longitude);
                                      });
                                    });
                                  }
                              ),
                              IconButton(
                                  color: WHITE_COLOR,
                                  icon: Icon(Icons.location_on),
                                  onPressed: (){
                                    setState(() {
                                      index = 2;
                                      _isSettingMarker = true;
                                      _isShowNewCamp = false;
                                      _isShowInformationMarkers = true;
                                    });
                                  }
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  controller: _shopController,
                                  style: TextStyle(color: WHITE_COLOR),
                                  textAlign: TextAlign.left,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    hintText: 'Bolt...',
                                    hintStyle: TextStyle(color: WHITER_COLOR),
                                    contentPadding: EdgeInsets.all(16),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITER_COLOR),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITE_COLOR),
                                    ),
                                    prefixIcon: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(Icons.shopping_cart, color: _campInformationMarkers[3].position != null ? TENT_COLOR : WHITE_COLOR),
                                    ),
                                  ),

                                ),
                              ),
                              IconButton(
                                  color: WHITE_COLOR,
                                  icon: Icon(Icons.my_location),
                                  onPressed: () async {
                                    await Geolocator.getCurrentPosition().then((value){
                                      setState(() {
                                        _campInformationMarkers[3].position = LatLng(value.latitude, value.longitude);
                                      });
                                    });
                                  }
                              ),
                              IconButton(
                                  color: WHITE_COLOR,
                                  icon: Icon(Icons.location_on),
                                  onPressed: (){
                                    setState(() {
                                      index = 3;
                                      _isSettingMarker = true;
                                      _isShowNewCamp = false;
                                      _isShowInformationMarkers = true;
                                    });
                                  }
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  controller: _playGroundController,
                                  style: TextStyle(color: WHITE_COLOR),
                                  textAlign: TextAlign.left,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    hintText: 'Sportolási lehetoség...',
                                    hintStyle: TextStyle(color: WHITER_COLOR),
                                    contentPadding: EdgeInsets.all(16),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITER_COLOR),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITE_COLOR),
                                    ),
                                    prefixIcon: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(Icons.sports_volleyball, color: _campInformationMarkers[4].position != null ? TENT_COLOR : WHITE_COLOR),
                                    ),
                                  ),

                                ),
                              ),
                              IconButton(
                                  color: WHITE_COLOR,
                                  icon: Icon(Icons.my_location),
                                  onPressed: () async {
                                    await Geolocator.getCurrentPosition().then((value){
                                      setState(() {
                                        _campInformationMarkers[4].position = LatLng(value.latitude, value.longitude);
                                      });
                                    });
                                  }
                              ),
                              IconButton(
                                  color: WHITE_COLOR,
                                  icon: Icon(Icons.location_on),
                                  onPressed: (){
                                    setState(() {
                                      index = 4;
                                      _isSettingMarker = true;
                                      _isShowNewCamp = false;
                                      _isShowInformationMarkers = true;
                                    });
                                  }
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  controller: _priceController,
                                  style: TextStyle(color: WHITE_COLOR),
                                  textAlign: TextAlign.left,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    hintText: 'Éjszaka/fő...',
                                    hintStyle: TextStyle(color: WHITER_COLOR),
                                    contentPadding: EdgeInsets.all(16),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITER_COLOR),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITE_COLOR),
                                    ),
                                    prefixIcon: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(Icons.attach_money, color: WHITE_COLOR),
                                    ),
                                  ),

                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  controller: _contactController,
                                  style: TextStyle(color: WHITE_COLOR),
                                  textAlign: TextAlign.left,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    hintText: 'Elérhetőség...',
                                    hintStyle: TextStyle(color: WHITER_COLOR),
                                    contentPadding: EdgeInsets.all(16),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITER_COLOR),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: WHITE_COLOR),
                                    ),
                                    prefixIcon: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(Icons.phone, color: WHITE_COLOR),
                                    ),
                                  ),

                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: pice/2,),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: ConstrainedBox(
                              constraints: BoxConstraints(
                                  minWidth: MediaQuery.of(context).size.width/3
                              ),
                              child: RoundedButton(
                                  title: 'Mentés',
                                  color: TENT_COLOR,
                                  onPressed: () {
                                    _campBloc.add(UpdateCampLocal(camp: Camp(
                                        campName: _campNameController.value.text,
                                        campName_lat: _campInformationMarkers[0].position != null ? _campInformationMarkers[0].position.latitude : null,
                                        campName_lon: _campInformationMarkers[0].position != null ? _campInformationMarkers[0].position.longitude : null,
                                        drinkWater: _drinkWaterController.value.text,
                                        drinkWater_lat: _campInformationMarkers[1].position != null ? _campInformationMarkers[1].position.latitude : null,
                                        drinkWater_lon: _campInformationMarkers[1].position != null ? _campInformationMarkers[1].position.longitude : null,
                                        bathWater: _bathWaterController.value.text,
                                        bathWater_lat: _campInformationMarkers[2].position != null ? _campInformationMarkers[2].position.latitude : null,
                                        bathWater_lon: _campInformationMarkers[2].position != null ? _campInformationMarkers[2].position.longitude : null,
                                        shop: _shopController.value.text,
                                        shop_lat: _campInformationMarkers[3].position != null ? _campInformationMarkers[3].position.latitude : null,
                                        shop_lon: _campInformationMarkers[3].position != null ? _campInformationMarkers[3].position.longitude : null,
                                        playground: _playGroundController.value.text,
                                        playground_lat: _campInformationMarkers[4].position != null ? _campInformationMarkers[4].position.latitude : null,
                                        playground_lon: _campInformationMarkers[4].position != null ? _campInformationMarkers[4].position.longitude : null,
                                      contact: _contactController.value.text,
                                      price: _priceController.value.text.toString(),
                                      rate: Rate(score: 0, rated: 0)
                                    )));
                                  }
                              )
                          ),
                        ),
                        SizedBox(height: pice/5,),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                                minWidth: MediaQuery.of(context).size.width/3
                            ),
                            child: RoundedButton(
                                title: 'Feltöltés',
                                color: _isConnection ? TENT_COLOR : DISABLE_COLOR,
                                onPressed: (){
                                  _campBloc.add(UpdateCampCloud(camp: Camp(
                                      campName: _campNameController.value.text,
                                      campName_lat: _campInformationMarkers[0].position != null ? _campInformationMarkers[0].position.latitude : null,
                                      campName_lon: _campInformationMarkers[0].position != null ? _campInformationMarkers[0].position.longitude : null,
                                      drinkWater: _drinkWaterController.value.text,
                                      drinkWater_lat: _campInformationMarkers[1].position != null ? _campInformationMarkers[1].position.latitude : null,
                                      drinkWater_lon: _campInformationMarkers[1].position != null ? _campInformationMarkers[1].position.longitude : null,
                                      bathWater: _bathWaterController.value.text,
                                      bathWater_lat: _campInformationMarkers[2].position != null ? _campInformationMarkers[2].position.latitude : null,
                                      bathWater_lon: _campInformationMarkers[2].position != null ? _campInformationMarkers[2].position.longitude : null,
                                      shop: _shopController.value.text,
                                      shop_lat: _campInformationMarkers[3].position != null ? _campInformationMarkers[3].position.latitude : null,
                                      shop_lon: _campInformationMarkers[3].position != null ? _campInformationMarkers[3].position.longitude : null,
                                      playground: _playGroundController.value.text,
                                      playground_lat: _campInformationMarkers[4].position != null ? _campInformationMarkers[4].position.latitude : null,
                                      playground_lon: _campInformationMarkers[4].position != null ? _campInformationMarkers[4].position.longitude : null,
                                      contact: _contactController.value.text,
                                      price: _priceController.value.text.toString(),
                                      rate: Rate(score: 0, rated: 0)
                                  )));
                                }
                            ),
                          ),
                        ),
                        SizedBox(height: pice/5,),
                      ],
                    ),
                );
              }
              throw 'Not implemented';
            }),
      ),
    );
  }

  void _moveCameraCenter(LatLng destLocation, double destZoom) {
    final _latTween = Tween<double>(
        begin: _mapController.center.latitude, end: destLocation.latitude);
    final _lngTween = Tween<double>(
        begin: _mapController.center.longitude, end: destLocation.longitude);
    final _zoomTween = Tween<double>(begin: _mapController.zoom, end: destZoom);

    var controller = AnimationController(
        duration: const Duration(milliseconds: 1200), vsync: this);
    Animation<double> animation =
    CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);

    controller.addListener(() {
      _mapController.move(
          LatLng(_latTween.evaluate(animation), _lngTween.evaluate(animation)),
          _zoomTween.evaluate(animation));
    });

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.dispose();
      } else if (status == AnimationStatus.dismissed) {
        controller.dispose();
      }
    });

    controller.forward();
  }

}