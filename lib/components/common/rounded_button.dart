import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:szsakd/utils/widget_settings.dart';

class RoundedButton extends StatefulWidget{
  String title;
  Color color;
  Function onPressed;
  IconData icon;
  RoundedButton({this.title,this.color,this.onPressed,this.icon});
  @override
  _RoundedButton createState() => _RoundedButton();
}

class _RoundedButton extends State<RoundedButton>{
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(ROUNDED_PADDING),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          widget.icon != null ? Icon(widget.icon) : Container(),
          Text('${widget.title}'),
        ],
      ),
      color: widget.color,
      onPressed: () => widget.onPressed(),
    );
  }
}