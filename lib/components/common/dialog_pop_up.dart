import 'package:flutter/material.dart';
import 'package:szsakd/utils/widget_settings.dart';

class DialogPopup extends StatelessWidget{

  final String title;
  final String message;
  final List<Widget> actions;
  DialogPopup({@required this.title, @required this.actions, @required this.message});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(ROUNDED_PADDING))
      ),
      title: Text(title),
      content: Text(message),
      actions: actions,
    );
  }
}