import 'dart:async';
import 'dart:io';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';
import 'package:szsakd/bloc/hiking/hiking_bloc.dart';
import 'package:szsakd/bloc/hiking/hiking_event.dart';
import 'package:szsakd/bloc/hiking/hiking_state.dart';
import 'package:szsakd/components/common/rounded_button.dart';
import 'package:szsakd/components/common/dialog_pop_up.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/model/hiking_model.dart';
import 'package:szsakd/model/latilongi_model.dart';
import 'package:szsakd/model/polypoints_model.dart';
import 'package:szsakd/utils/converter.dart';
import 'package:szsakd/utils/map_settings.dart';
import 'package:szsakd/utils/colors.dart';
import 'package:szsakd/utils/widget_settings.dart';

class TrackerPage extends StatefulWidget {
  bool isRecordedShow;
  TrackerPage({this.isRecordedShow});
  @override
  _TrackerPageState createState() => _TrackerPageState();
}

class _TrackerPageState extends State<TrackerPage> with WidgetsBindingObserver, TickerProviderStateMixin{

  AppLifecycleState notif;
  @override
  void didChangeAppLifecycleState(AppLifecycleState state){
    switch(state){
      case AppLifecycleState.resumed:
        if(_isRecording) {
          stopService();
        }
        break;
      case AppLifecycleState.inactive:
        if(_isRecording) {
          stopListener();
          startService();
        }
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
    }
  }

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<LatLng> points;
  List<PolyPoints> polypoints;

  StreamSubscription<Position> positionStream;
  double _distance;
  bool _isRecording;

  bool _isFinished;
  bool _isSavingPanelShow;

  LatLng _current;
  MapController _mapController;

  List<Hiking> _hikingsCloud;
  List<Hiking> _filteredHikingsCloud;
  var ranges;
  bool _isShowCloudHiking;

  StreamSubscription<DataConnectionStatus> listener;
  bool _isConnection;

  bool _notSavedNewHiking;
  HikingBloc _hikingBloc;
  TextEditingController _fromController = TextEditingController();
  TextEditingController _toController = TextEditingController();
  int _point;

  TextEditingController _hikingController = TextEditingController();

  bool disposed;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    points = <LatLng>[];
    polypoints = [];
    _distance = 0;
    _isRecording = false;
    _isFinished = false;
    _isSavingPanelShow = false;

    _hikingsCloud = [];
    _filteredHikingsCloud = [];
    ranges = RangeValues(0, 20);
    filter();
    _isShowCloudHiking = false;

    _mapController = MapController();
    _mapController.onReady.then((value){
    });
    _positionCurrent();

    _isConnection = false;
    checkConnection();

    _notSavedNewHiking=false;
    _hikingBloc = BlocProvider.of<HikingBloc>(context);
    if(widget.isRecordedShow){
      _hikingBloc.add(GetHikingCloud());
    }else {
      if (_hikingBloc.state is HikingLocalLoadedState) {
        if (_hikingBloc.state.hiking != null) {
          initHikingSetting(_hikingBloc.state.hiking);
        }
      } else {
        _hikingBloc.add(GetHikingLocal());
      }
    }
    disposed = false;
  }

  @override
  void dispose() {
    _isRecording = false;
    disposed = true;
    stopListener();
    super.dispose();
  }

  Future<void> startService() async {
    if(Platform.isAndroid){
      var methodChannel = MethodChannel('com.rrm.szakd');
      String data = await methodChannel.invokeMethod("startService");
    }
  }

  Future<void> stopService() async {
    if(Platform.isAndroid){
      var methodChannel = MethodChannel('com.rrm.szakd');
      final List<dynamic> data = await methodChannel
          .invokeMethod("stopService");
      List<LatiLongi> cont = data.map((e) => LatiLongi.fromMap(e)).toList();
      cont.forEach((position) {
        _distance+= Converter.meterToKm(Geolocator.distanceBetween(points.last.latitude, points.last.longitude, position.latitude, position.longitude));
        _distance = double.parse((_distance).toStringAsFixed(2));
        if(points.isNotEmpty) {
          polypoints.add(PolyPoints(points.last,LatLng(position.latitude,position.longitude)));
        }
        points = [...points,LatLng(position.latitude,position.longitude)];
      });
      setUpListener(true);
    }
  }
  
  void saveTourFromLatiLongi(List<LatiLongi> tour, bool center){
    List<LatiLongi> actLatiLongi = [];
    setState(() {
        polypoints.clear();
        tour.forEach((act) {
          if(act == tour.first){
            actLatiLongi.add(act);
          }else{
            polypoints.add(PolyPoints(LatLng(actLatiLongi.last.latitude,actLatiLongi.last.longitude),LatLng(act.latitude,act.longitude)));
            actLatiLongi.add(act);
          }
        });
        if(center){
          if(tour.isNotEmpty) {
            if(tour.first.longitude != null && tour.first.longitude != null) {
              MapSetting.moveCameraCenter(
                  LatLng(tour.first.latitude, tour.first.longitude), 15, this,
                  _mapController);
            }
          }
        }
    });
  }

  Future<void> _positionCurrent() async{
    await Geolocator.getCurrentPosition().then((value){
      setState(() {
        _current = LatLng(value.latitude, value.longitude);
        MapSetting.moveCameraCenter(_current,15,this,_mapController);
      });
    });
  }

  checkConnection() async {
    listener = DataConnectionChecker().onStatusChange.listen((status) {
      switch (status) {
        case DataConnectionStatus.connected:
          final snackBar = SnackBar(content: Text('Van internet kapcsolat'), backgroundColor: GREEN_COLOR,);
          _scaffoldKey.currentState.showSnackBar(snackBar);
          setState(() {
            _isConnection = true;
          });
          break;
        case DataConnectionStatus.disconnected:
          final snackBar = SnackBar(content: Text('Nincs internet kapcsolat'),backgroundColor: RED_COLOR,);
          _scaffoldKey.currentState.showSnackBar(snackBar);
          setState(() {
            _isConnection = false;
          });
          break;
      }
    });
    return await DataConnectionChecker().connectionStatus;
  }

  void initHikingSetting(Hiking hiking){
    setState(() {
      _fromController..text = hiking.from;
      _toController..text = hiking.to;
      _distance = hiking.distance;
      _point = hiking.point;
      _notSavedNewHiking = true;
      saveTourFromLatiLongi(hiking.trip,false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          child: Stack(
            children: [
              FlutterMap(
                mapController: _mapController,
                options: MapOptions(
                  center: _current != null ? _current : LatLng(47.49801, 19.03991),
                  zoom: 8.0,
                  maxZoom: 17.0,
                  minZoom: 3,
                ),
                layers: [
                  TileLayerOptions(
                      tileProvider: NonCachingNetworkTileProvider(),
                      urlTemplate: "https://tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png",
                      subdomains: ['a','b','c']
                  ),
                  PolylineLayerOptions(
                    polylines: polypoints.isNotEmpty ? polypoints.map((e){
                      return Polyline(
                          points: [e.start,e.dest],
                          strokeWidth: 3,
                          color: RED_COLOR,
                      );
                    }).toList() : [],
                  ),
                  MarkerLayerOptions(
                    markers: [
                      Marker(
                          point: _current,
                          width: 30,
                          height: 30,
                          builder: (ctx) =>
                              IconButton(
                                icon: Icon(Icons.circle,color: ACCENTBLUE_COLOR,),
                              )
                      )
                    ],
                  ),
                ],
              ),
              _isSavingPanelShow ? Container() : Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: PRIMARY_COLOR,
                      borderRadius: BorderRadius.all(Radius.circular(ROUNDED_PADDING))
                  ),
                  height: MediaQuery.of(context).size.height/8,
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: Row(
                      children: [
                        Expanded(flex:1, child: Align(alignment: Alignment.center, child: Text('Távolság', style: TextStyle(color: WHITE_COLOR), textScaleFactor: 1.4,))),
                        Expanded(flex:1,child: Align(alignment: Alignment.center, child: Text('$_distance km', style: TextStyle(color: WHITE_COLOR))))
                      ],
                    ),
                  ),
                ),
              ),
              (widget.isRecordedShow && !_isShowCloudHiking) ? _getListWidget() : Container(),
              _isSavingPanelShow ? _getSavePanelWidget() : Container()
            ],
          ),
        ),
        floatingActionButton: (widget.isRecordedShow || _isSavingPanelShow) ? null : Column(
          children: <Widget>[
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: FloatingActionButton(
                  heroTag: 'center',
                  backgroundColor: WHITE_COLOR,
                  child: Icon(Icons.my_location,color: TENT_COLOR,),
                  onPressed: (){
                    _positionCurrent();
                  },),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                 _notSavedNewHiking ? Padding(
                  padding: const EdgeInsets.only(left: 25.0,right:8.0,bottom:8.0,top: 8.0),
                  child: FloatingActionButton(
                    heroTag: 'notsaved',
                    backgroundColor: PRIMARY_COLOR,
                    child: Icon(Icons.directions_walk,color: WHITE_COLOR,),
                    onPressed: (){
                      setState(() {
                        _isSavingPanelShow = true;
                      });
                    },),
                ): BlocConsumer<HikingBloc, HikingState>(
                     listener: (context, state){
                       if(state is HikingLocalLoadedState) {
                         if (state.hiking != null) {
                           setState(() {
                             _notSavedNewHiking = true;
                             _fromController..text = state.hiking.from;
                             _toController..text = state.hiking.to;
                             _distance = state.hiking.distance;
                           });
                         }
                       }
                     },
                     builder: (context, state){return Container();}),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FloatingActionButton(
                    heroTag: 'startRecord2',
                    backgroundColor: TENT_COLOR,
                    child: _isFinished ? Icon(Icons.save_alt_sharp) : (_isRecording ? Icon(Icons.stop) : Icon(Icons.play_arrow)),
                    onPressed: () async{
                      if(_isFinished){
                        saveTour();
                      }else {
                        if (_isRecording) {
                          stopRecord();
                        } else {
                          if(_notSavedNewHiking) {
                            showNotNewHikingAlertDialog(context);
                          }else{
                            startRecord();
                          }
                        }
                      }
                    },
                    tooltip: "Toggle Foreground Service On/Off",
                  ),
                ),
              ],
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.end,
        ),
      ),
      onWillPop: () async{
        if(widget.isRecordedShow == false){
          if(_isSavingPanelShow == true){
            setState(() {
              _isSavingPanelShow = false;
            });
            return false;
          }
          if(_isRecording == true) {
            showNotSavedAlertDialog(context);
          }else {
            if (_notSavedNewHiking) {
              Navigator.pop(_scaffoldKey.currentContext, true);
              return true;
            } else {
              Navigator.pop(_scaffoldKey.currentContext, false);
              return true;
            }
          }
        }else{
          if(_isShowCloudHiking){
            setState(() {
              _isShowCloudHiking = false;
            });
            return false;
          }
        }
        return true;
      },
    );
  }

  showCloudUpdateAlertDialog(BuildContext context) {
    Widget okButton = RoundedButton(
      color: TENT_COLOR,
      title: "ok",
      onPressed: () {
        Navigator.pop(context);
      },
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogPopup(
            title: 'Sikeres feltöltés!',
            message: 'A kirándulást sikeresen feltöltésre került.',
            actions:[okButton]);
      },
    );
  }

  showNotSavedAlertDialog(BuildContext context) {
    Widget okButton = RoundedButton(
      color: TENT_COLOR,
      title: "Vissza",
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget exitButton = RoundedButton(
      color: TENT_COLOR,
        title: "Kilépés",
        onPressed: () {
          Navigator.pop(context);
          Navigator.pop(context);
          setState(() {
            _isSavingPanelShow = false;
          });
        },
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogPopup(title: 'Nem mentett kirándulás!', actions:[okButton, exitButton] , message: 'Mentse el kirándulását mielőtt kilépne!');
      },
    );
  }

  showNotNewHikingAlertDialog(BuildContext context) {
    Widget exitButton = RoundedButton(
      color: TENT_COLOR,
      title: "Ok",
      onPressed: () {
        Navigator.pop(context);
        setState(() {
          //_isSavingPanelShow = true;
        });
      },
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogPopup(
            title: "Van nem feltöltött kirándulás!",
            message: "Töltse fel vagy törölje a kirándulás mielőtt újat felvenne!",
            actions:[exitButton]);
      },
    );
  }

  Widget _getListWidget(){
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
              color: PRIMARY_COLOR,
              borderRadius: BorderRadius.all(Radius.circular(ROUNDED_PADDING))
          ),
          height: MediaQuery.of(context).size.height,
          child: BlocConsumer<HikingBloc, HikingState>(
              listener: (context, state){
                if(state is HikingCloudLoadedState){
                  if(state.hikings.isNotEmpty){
                    setState(() {
                      _hikingsCloud = state.hikings;
                      filter();
                    });
                  }
                }
              },
              builder: (context, state){
                if(state is HikingCloudLoadingState){
                  return Center(child:  Align(alignment: Alignment.center, child: CircularProgressIndicator()));
                }
                if(state is HikingCloudLoadedState){
                  return  Column(
                    children: [
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: _hikingController,
                            onChanged: (_){
                              filter();
                            },
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(8.0),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(ROUNDED_PADDING),
                                borderSide: BorderSide(
                                    color: WHITE_COLOR
                                ),
                              ),
                              focusedBorder:  OutlineInputBorder(
                                borderRadius: BorderRadius.circular(ROUNDED_PADDING),
                                borderSide: BorderSide(
                                    color: WHITE_COLOR
                                ),
                              ),
                              enabledBorder:  OutlineInputBorder(
                                borderRadius: BorderRadius.circular(ROUNDED_PADDING),
                                borderSide: BorderSide(
                                    color: WHITE_COLOR
                                ),
                              ),
                              hintText: "Kirándulások...",
                              fillColor: WHITE_COLOR,
                              filled: true,
                              prefixIcon: Padding(
                                padding: EdgeInsets.all(2.0), // add padding to adjust icon
                                child: Icon(Icons.search),
                              ),
                            ),
                          ),
                        ),
                      ),
                      RangeSlider(
                          values: ranges,
                          onChanged: (RangeValues newRange){
                            setState(() {
                              ranges = newRange;
                            });
                            filter();
                          },
                          min: 0,
                          max: 50,
                          divisions: 50,
                          labels: RangeLabels('${(ranges.start).toInt()} km', ranges.end.toInt() == 50 ? '50+ km' : '${(ranges.end).toInt()} km')

                      ),
                      Flexible(
                        child: ListView.builder(
                          itemCount: _filteredHikingsCloud.length,
                          itemBuilder: (_, i) => Container(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(ROUNDED_PADDING),
                                      border: Border.all(
                                          width: 2.0
                                      ),
                                      color: WHITE_COLOR
                                  ),
                                  child: ListTile(
                                    title: Row(
                                        children: [
                                          Expanded(
                                              flex: 6,
                                              child: Text('${_filteredHikingsCloud[i].from} - ${_filteredHikingsCloud[i].to}',textScaleFactor: 1.1, overflow: TextOverflow.ellipsis)
                                          ),
                                          Expanded(
                                              flex: 2,
                                              child: Text('${_filteredHikingsCloud[i].distance}')
                                          ),
                                          Expanded(
                                              flex: 1,
                                              child: Text('km')
                                          ),
                                        ]
                                    ),
                                    subtitle: Text('${_filteredHikingsCloud[i].teamId}'),
                                    onTap: (){
                                      setState(() {
                                        _isShowCloudHiking = true;
                                        _distance = _filteredHikingsCloud[i].distance;
                                        saveTourFromLatiLongi(_filteredHikingsCloud[i].trip, true);
                                      });
                                    },
                                  ),
                                ),
                              )
                          ),
                        ),
                      ),
                    ],
                  );
                }
                //throw 'Not implemented';
                return Container();
              })
        ),
      ),
    );
  }

  void filter(){
    final filtered = _hikingsCloud.where((e) =>
      (e.from.toLowerCase().contains(_hikingController.value.text.toLowerCase()) || e.to.toLowerCase().contains(_hikingController.value.text.toLowerCase()) || e.teamId.toLowerCase().contains(_hikingController.value.text.toLowerCase())) &&
          ((e.distance >= ranges.start.toInt() && e.distance <= ranges.end.toInt()) || (ranges.end.toInt() == 50 && e.distance >= 50 ))).toList();
    setState(() {
      _filteredHikingsCloud = filtered;
      _filteredHikingsCloud.sort((a,b) => a.distance.compareTo(b.distance));
    });
  }

  Widget _getSavePanelWidget(){
    final pice = MediaQuery.of(context).size.height/10;
    return Padding(
      padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
              color: PRIMARY_COLOR,
              borderRadius: BorderRadius.all(Radius.circular(ROUNDED_PADDING))
          ),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: BlocConsumer<HikingBloc, HikingState>(
              listener: (context, state){
                if(state is HikingLocalLoadedState) {
                  if(state.delete){
                    setState(() {
                      clearRecord();
                    });
                  }
                }
                if(state is HikingCloudLoadedState){
                  clearRecord();
                  showCloudUpdateAlertDialog(_scaffoldKey.currentContext);
                }
              },
              builder: (context, state){
                if(state is  HikingLocalLoadingState || state is  HikingCloudLoadingState){
                  return Center(child:  Align(alignment: Alignment.center, child: CircularProgressIndicator()));
                }
                if(state is HikingLocalLoadedState || state is UninitializedState || state is HikingCloudLoadedState) {
                  return SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(height: pice / 2,),
                        Row(
                          children: [
                            IconButton(
                                icon: Icon(Icons.delete,color: WHITE_COLOR),
                                onPressed: (){
                                  setState(() {
                                    _hikingBloc.add(DeleteHikingLocal());
                                  });
                                }
                            ),
                            Expanded(
                              flex: 8,
                              child: Align(
                                  alignment: Alignment.center,
                                  child: Text('Gratulálok!',
                                    style: TextStyle(color: WHITE_COLOR),
                                    textScaleFactor: 2,)
                              ),
                            ),
                            IconButton(
                                icon: Icon(Icons.map,color: WHITE_COLOR),
                                onPressed: (){
                                  setState(() {
                                    _isSavingPanelShow = false;
                                  });
                                }
                            )
                          ],
                        ),
                        SizedBox(height: pice / 2,),
                        Align(
                            alignment: Alignment.center,
                            child: Text('Mentsd el a kirándulásod!',
                              style: TextStyle(color: WHITE_COLOR),
                              textScaleFactor: 1.3,)
                        ),
                        SizedBox(height: pice / 2,),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextField(
                            controller: _fromController,
                            style: TextStyle(color: WHITE_COLOR),
                            textAlign: TextAlign.left,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: 'Honnan...',
                              hintStyle: TextStyle(color: WHITER_COLOR),
                              contentPadding: EdgeInsets.all(16),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: WHITER_COLOR),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: WHITE_COLOR),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextField(
                            controller: _toController,
                            style: TextStyle(color: WHITE_COLOR),
                            textAlign: TextAlign.left,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              hintText: 'Hova...',
                              hintStyle: TextStyle(color: WHITER_COLOR),
                              contentPadding: EdgeInsets.all(16),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: WHITER_COLOR),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: WHITE_COLOR),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: pice / 2,),
                        Align(alignment: Alignment.center,
                            child: Text('${_distance} km', textScaleFactor: 1.4,
                                style: TextStyle(
                                  color: WHITE_COLOR, decoration: TextDecoration
                                    .underline,))),
                        SizedBox(height: pice / 2,),
                        Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child: Align(alignment: Alignment.center,
                                    child: Text(
                                        'Jutalmad:', textScaleFactor: 1.5,
                                        style: TextStyle(color: WHITE_COLOR)))
                            ),
                            Expanded(
                                flex: 1,
                                child: Align(alignment: Alignment.center,
                                  child: Text('${_point} pont', textScaleFactor: 1.4,
                                      style: TextStyle(color: WHITE_COLOR)))
                            )
                          ],
                        ),
                        SizedBox(height: pice / 2,),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                                minWidth: MediaQuery
                                    .of(context)
                                    .size
                                    .width / 3
                            ),
                            child: RoundedButton(
                                title: 'Mentés',
                                color: TENT_COLOR,
                                onPressed: () {
                                  List<LatiLongi> lats = [];
                                  points.forEach((element) {
                                    lats.add(LatiLongi(latitude: element.latitude, longitude: element.longitude));
                                  });
                                  _hikingBloc.add(UpdateHikingLocal(hiking:
                                      Hiking(
                                          from: _fromController.value.text,
                                          to: _toController.value.text,
                                          distance: _distance,
                                          point: _point,
                                          trip: lats,
                                          teamId: Config.currentUser.teamId
                                      )
                                    )
                                  );
                                  setState(() {
                                    _notSavedNewHiking = true;
                                  });
                                }
                            ),
                          ),
                        ),
                        SizedBox(height: pice / 5,),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                                minWidth: MediaQuery
                                    .of(context)
                                    .size
                                    .width / 3
                            ),
                            child: RoundedButton(
                                title: 'Feltöltés',
                                color: _isConnection
                                    ? TENT_COLOR
                                    : DISABLE_COLOR,
                                onPressed: () {
                                    List<LatiLongi> lats = [];
                                    points.forEach((element) {
                                      lats.add(LatiLongi(latitude: element.latitude, longitude: element.longitude));
                                    });
                                    _hikingBloc.add(UpdateHikingCloud(hiking:
                                      Hiking(
                                          from: _fromController.value.text,
                                          to: _toController.value.text,
                                          distance: _distance,
                                          point: _point,
                                          trip: lats,
                                          teamId: Config.currentUser.teamId
                                        )
                                      )
                                    );

                                }
                            ),
                          ),
                        ),
                        SizedBox(height: pice / 2,),
                      ],
                    ),
                  );
                }
                throw 'Not implemented';
              }),
        ),
      );
  }

  void stopListener(){
    if(positionStream!=null){
      positionStream.cancel();
    }
  }
  void setUpListener(bool _firstPoint){
    bool firstPoint = _firstPoint;
    positionStream = Geolocator.getPositionStream(distanceFilter: 3, desiredAccuracy: LocationAccuracy.high).listen(
            (Position position) {
              if(!disposed) {
                setState(() {
                  if (firstPoint) {
                    _distance += Converter.meterToKm(Geolocator.distanceBetween(
                        _current.latitude, _current.longitude,
                        position.latitude,
                        position.longitude));
                    _distance = double.parse((_distance).toStringAsFixed(2));
                  }
                  firstPoint = true;
                  _current.latitude = position.latitude;
                  _current.longitude = position.longitude;
                  if (points.isNotEmpty) {
                    polypoints.add(PolyPoints(points.last,
                        LatLng(position.latitude, position.longitude)));
                  }
                  points =
                  [...points, LatLng(position.latitude, position.longitude)];

                  _current = LatLng(position.latitude, position.longitude);
                  MapSetting.moveCameraCenter(_current,_mapController.zoom,this,_mapController);
                });
              }

        });
  }

  void startRecord(){
    setState(() {
      _isRecording = true;
    });
    bool firstPoint = false;
    setUpListener(firstPoint);
  }

  void stopRecord(){
    setState(() {
      _isRecording = false;
      _isFinished = true;
    });
    if(positionStream!=null){
      positionStream.cancel();
    }
  }

  void clearRecord(){
    setState(() {
      _distance = 0;
      points.clear();
      polypoints.clear();
      _fromController..text = '';
      _toController..text = '';
      _point = 0;
      _notSavedNewHiking = false;
      _isSavingPanelShow = false;
      _isFinished = false;
      _isRecording = false;
    });
  }

  void saveTour(){
    setState(() {
      _point = Converter.kmToPoint(_distance);
      _isSavingPanelShow = true;
    });
  }
}