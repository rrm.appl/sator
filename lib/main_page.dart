import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:szsakd/bloc/auth/auth_bloc.dart';
import 'package:szsakd/bloc/camp/camp_bloc.dart';
import 'package:szsakd/bloc/hiking/hiking_bloc.dart';
import 'package:szsakd/bloc/rank/rank_bloc.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/repos/auth/auth_repository.dart';
import 'package:szsakd/repos/camp/camp_repository.dart';
import 'package:szsakd/repos/camp/campcloud_service.dart';
import 'package:szsakd/repos/camp/camplocal_service.dart';
import 'package:szsakd/repos/hiking/hiking_repository.dart';
import 'package:szsakd/repos/hiking/hikingcloud_service.dart';
import 'package:szsakd/repos/hiking/hikinglocal_service.dart';
import 'package:szsakd/repos/rank/rank_repository.dart';
import 'package:szsakd/repos/rank/rank_service.dart';
import 'package:szsakd/utils/colors.dart';

import 'components/auth/auth_nav.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key key}): super(key: key);
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>{

  @override
  void initState() {
    Config.getFirebaseMessaing().configure(
      onMessage: (Map<String, dynamic> message) async {
        Config.notification.showNotification(message['notification']['title'],message['notification']['body']);
      },
      onLaunch: (Map<String, dynamic> message) async {
        Config.notification.showNotification(message['notification']['title'],message['notification']['body']);
      },
      onResume: (Map<String, dynamic> message) async {
        Config.notification.showNotification(message['notification']['title'],message['notification']['body']);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers : [
        RepositoryProvider<CampRepository>(
          create: (context) => CampRepository(CampLocalService(), CampCloudService()),
        ),
        RepositoryProvider<HikingRepository>(
          create: (context) => HikingRepository(HikingLocalService(), HikingCloudService()),
        ),
        RepositoryProvider<AuthRepository>(
          create: (context) => AuthRepository(firebaseAuth: Config.getFirebaseAuth()),
        ),
        RepositoryProvider<RankRepository>(
          create: (context) => RankRepository(RankService()),
        )
      ],
      child: MultiBlocProvider(
          providers: _getBlocProviders(),
          child: _buildMaterialApp()
      ),
    );
  }

  Widget _buildMaterialApp(){
    return MaterialApp(
        title: "Tent app",
        theme: ThemeData(
          primaryColor: PRIMARY_COLOR,
        ),
        initialRoute: '/',
        routes:{
          '/' : (context) =>  AuthNav()
        }
    );
  }

  List<BlocProvider> _getBlocProviders() {
    return [
      BlocProvider<CampBloc>(create: (context) => CampBloc(RepositoryProvider.of<CampRepository>(context))),
      BlocProvider<HikingBloc>(create: (context) => HikingBloc(RepositoryProvider.of<HikingRepository>(context))),
      BlocProvider<AuthBloc>(create: (context) => AuthBloc(RepositoryProvider.of<AuthRepository>(context))),
      BlocProvider<RankBloc>(create: (context) => RankBloc(RepositoryProvider.of<RankRepository>(context))),
    ];
  }
}