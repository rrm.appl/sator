import 'package:szsakd/model/camp_model.dart';
import 'package:szsakd/repos/camp/campcloud_service.dart';
import 'package:szsakd/repos/camp/camplocal_service.dart';


class CampRepository{
  final CampLocalService campLocalService;
  final CampCloudService campCloudService;
  CampRepository(this.campLocalService, this.campCloudService);

  Future<bool> saveCampLocal(Camp camp) async {
    try {
      await campLocalService.saveCampLocal(camp);
      return true;
    }
    catch(e){
      print(e);
      return false;
    }
  }

  Future<Camp> getCampLocal() async {
    try {
      Camp camp = await campLocalService.getCampLocal();
      return camp;
    }
    catch(e){
      print(e);
      return null;
    }
  }

  Future<bool> deleteCampLocal() async {
    try {
      await campLocalService.deleteCampLocal();
      return true;
    }
    catch(e){
      print(e);
      return false;
    }
  }

  Future<bool> saveCampCloud(Camp camp) async {
    try {
      await campCloudService.saveCampCloud(camp);
      return true;
    }
    catch(e){
      print(e);
      return false;
    }
  }

  Future<List<Camp>> getCampCloud() async {
    try {
      List<Camp> camps = await campCloudService.getCampCloud();
      return camps;
    }
    catch(e){
      print(e);
      return null;
    }
  }
}