import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/model/camp_model.dart';
import 'package:szsakd/repos/rank/rank_service.dart';
import 'package:szsakd/utils/converter.dart';

class CampCloudService{
  static const String COLLECTION = "camps";

  Future<void> saveCampCloud(Camp camp) async {
    await RankService.setTeamPoint(Config.currentUser.teamId, Converter.pointPerCamp);
    DocumentReference docRef = await Config.getFireStore().collection(COLLECTION).add(camp.toMap());
    return await Config.getFireStore().collection(COLLECTION).doc(docRef.id).update({'uid': docRef.id});
  }

  Future<List<Camp>> getCampCloud() async {
    try {
      List<Camp> camps = await Config.getFireStore().collection(COLLECTION).get().then((QuerySnapshot querySnapshot)
      {
        List<QueryDocumentSnapshot> data = querySnapshot.docs;
        List<Camp> camps = data.map((e) => Camp.fromCloudMap(e)).toList();
        return camps;
      });
      return camps;
    }
    catch(e){
      print(e);
      return null;
    }
  }
}