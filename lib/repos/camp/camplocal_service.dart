import 'package:hive/hive.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/model/camp_model.dart';

class CampLocalService{
  static const String BOX_KEY = 'camplocal';

  Future<void> saveCampLocal(Camp camp) async {
    final box = await Hive.openBox(BOX_KEY);
    return box.put(Config.currentUser.uid, camp.toMap());
  }

  Future<Camp> getCampLocal() async {
    try {
      final box = await Hive.openBox(BOX_KEY);
      final boxValue = box.get(Config.currentUser.uid);
      if(boxValue != null){
        return Camp.fromMap(Map<String, dynamic>.from(boxValue));
      }
      return null;
    }
    catch(e){
      print(e);
      return null;
    }
  }

  Future<bool> deleteCampLocal() async {
    try {
      final box = await Hive.openBox(BOX_KEY);
      final boxValue = box.get(Config.currentUser.uid);
      if(boxValue != null){
          box.put(Config.currentUser.uid, null);
          return true;
      }
      return false;
    }
    catch(e){
      print(e);
      return false;
    }
  }
}