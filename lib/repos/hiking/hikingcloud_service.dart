import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/model/hiking_model.dart';
import 'package:szsakd/repos/rank/rank_service.dart';

class HikingCloudService{
  static const String COLLECTION = "hikings";

  Future<void> saveHikingCloud(Hiking hiking) async {
    await RankService.setTeamPoint(Config.currentUser.teamId, hiking.point);
    return await Config.getFireStore().collection(COLLECTION).add(hiking.toMap()).catchError((e)=>debugPrint(e));
  }

  Future<List<Hiking>> getHikingCloud() async {
    try {
      List<Hiking> hikings = await Config.getFireStore().collection(COLLECTION).get().then((QuerySnapshot querySnapshot)
      {
          List<QueryDocumentSnapshot> data = querySnapshot.docs;
          List<Hiking> hikings = data.map((e) => Hiking.fromCloudMap(e)).toList();
          return hikings;
      });
      return hikings;
    }
    catch(e){
      print(e);
      return null;
    }
  }
}