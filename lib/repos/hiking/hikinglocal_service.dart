import 'package:hive/hive.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/model/hiking_model.dart';

class HikingLocalService{
  static const String BOX_KEY = 'hikinglocal';

  Future<void> saveHikingLocal(Hiking hiking) async {
    final box = await Hive.openBox(BOX_KEY);
    return box.put(Config.currentUser.uid, hiking.toMap());
  }

  Future<Hiking> getHikingLocal() async {
    try {
      final box = await Hive.openBox(BOX_KEY);
      final boxValue = box.get(Config.currentUser.uid);
      if(boxValue != null){
        return Hiking.fromMap(Map<String, dynamic>.from(boxValue));
      }
      return null;
    }
    catch(e){
      print(e);
      return null;
    }
  }

  Future<bool> deleteHikingLocal() async {
    try {
      final box = await Hive.openBox(BOX_KEY);
      final boxValue = box.get(Config.currentUser.uid);
      if(boxValue != null){
        box.put(Config.currentUser.uid, null);
        return true;
      }
      return false;
    }
    catch(e){
      print(e);
      return false;
    }
  }

}