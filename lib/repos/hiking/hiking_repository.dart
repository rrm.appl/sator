import 'package:szsakd/model/hiking_model.dart';
import 'package:szsakd/repos/hiking/hikingcloud_service.dart';
import 'package:szsakd/repos/hiking/hikinglocal_service.dart';

class HikingRepository{
  final HikingLocalService hikingLocalService;
  final HikingCloudService hikingCloudService;
  HikingRepository(this.hikingLocalService, this.hikingCloudService);

  Future<bool> saveHikingLocal(Hiking hiking) async {
    try {
      await hikingLocalService.saveHikingLocal(hiking);
      return true;
    }
    catch(e){
      print(e);
      return false;
    }
  }

  Future<Hiking> getHikingLocal() async {
    try {
      Hiking camp = await hikingLocalService.getHikingLocal();
      return camp;
    }
    catch(e){
      print(e);
      return null;
    }
  }

  Future<bool> deleteHikingLocal() async {
    try {
      await hikingLocalService.deleteHikingLocal();
      return true;
    }
    catch(e){
      print(e);
      return false;
    }
  }

  Future<bool> saveHikingCloud(Hiking hiking) async {
    try {
      await hikingCloudService.saveHikingCloud(hiking);
      return true;
    }
    catch(e){
      print(e);
      return false;
    }
  }

  Future<List<Hiking>> getHikingCloud() async {
    try {
      List<Hiking> hikings = await hikingCloudService.getHikingCloud();
      return hikings;
    }
    catch(e){
      print(e);
      return null;
    }
  }
}