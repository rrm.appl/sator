import 'package:szsakd/model/team_model.dart';
import 'package:szsakd/model/team_rank_model.dart';
import 'package:szsakd/repos/rank/rank_service.dart';

class RankRepository{
  final RankService rankService;
  RankRepository(this.rankService);

  Future<List<Team>> getTeams() async {
    try {
      List<Team> teams = await rankService.getTeams();
      return teams;
    }
    catch(e){
      print(e);
      return null;
    }
  }

  Future<List<TeamRank>> getTeamRanks() async {
    try {
      List<TeamRank> teamRanks = await rankService.getTeamRanks();
      return teamRanks;
    }
    catch(e){
      print(e);
      return null;
    }
  }
}