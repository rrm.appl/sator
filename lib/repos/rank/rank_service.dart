import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/model/camp_model.dart';
import 'package:szsakd/model/hiking_model.dart';
import 'package:szsakd/model/rate_mode.dart';
import 'package:szsakd/model/team_model.dart';
import 'package:szsakd/model/team_rank_model.dart';

class RankService{
  static const String COLLECTION_TEAMS = "teams";
  static const String COLLECTION_HIKING = "hikings";
  static const String COLLECTION_CAMPS = "camps";

  Future<List<Team>> getTeams() async {
    try {
      List<Team> teams = await Config.getFireStore().collection(COLLECTION_TEAMS).get().then((QuerySnapshot querySnapshot)
      {
          List<QueryDocumentSnapshot> data = querySnapshot.docs;
          List<Team> teams = data.map((e) => Team.fromCloudMap(e)).toList();
          return teams;
      });
      return teams;
    }
    catch(e){
      print(e);
      return null;
    }
  }

  Future<List<TeamRank>> getTeamRanks() async {
    try {
      List<TeamRank> teamRanks = [];
      List<Team> teams = await Config.getFireStore().collection(COLLECTION_TEAMS).get().then((QuerySnapshot querySnapshot)
      {
        List<QueryDocumentSnapshot> data = querySnapshot.docs;
        List<Team> teams = data.map((e) => Team.fromCloudMap(e)).toList();
        return teams;
      });
      await Future.forEach((teams),(currTeam) async {
      List<Hiking> hikings = await Config.getFireStore().collection(COLLECTION_HIKING).where('teamId', isEqualTo: currTeam.id).get()
          .then((QuerySnapshot querySnapshot)
          {
            List<QueryDocumentSnapshot> data = querySnapshot.docs;
            List<Hiking> hikings = data.map((e) => Hiking.fromCloudMap(e)).toList();
            return hikings;
          });
      double allDistance = 0;
      hikings.forEach((currHiking) {
        allDistance += currHiking.distance;
      });
      teamRanks.add(
        TeamRank(
          name: currTeam.name,
          rank: currTeam.rank,
          allDistance: double.parse(allDistance.toStringAsFixed(2)),
          point: currTeam.point
        )
      );
      });
      return teamRanks;
    }
    catch(e){
      print(e);
      return null;
    }
  }

  static Future<String> getTeamName(String id) async {
    try {
      String name = await Config.getFireStore().collection(COLLECTION_TEAMS).doc(id).get().then((DocumentSnapshot documentSnapshot)
      {
        Team team = Team.fromMap(documentSnapshot.data());
        return team.name;
      });
      return name;
    }
    catch(e){
      print(e);
      return null;
    }
  }

  static Future<void> updateRating(String uid, int score) async{
    Camp camp = await Config.getFireStore().collection(COLLECTION_CAMPS).doc(uid).get().then((DocumentSnapshot documentSnapshot)
    {
      Camp camp = Camp.fromMap(documentSnapshot.data());
      return camp;
    });
    int all = camp.rate.score*camp.rate.rated + score;
    int new_rated = camp.rate.rated + 1;
    int new_score = (all / new_rated).round();
    Rate rate = Rate(score: new_score,rated: new_rated);
    FirebaseFirestore.instance.collection(COLLECTION_CAMPS).doc(uid).update({'rate': rate.toMap()});
  }

  static Future<void> setTeamPoint(String id, int _point) async {
    try {
      int point = await Config.getFireStore().collection(COLLECTION_TEAMS).doc(id).get().then((DocumentSnapshot documentSnapshot)
      {
        Team team = Team.fromMap(documentSnapshot.data());
        return team.point;
      });
      debugPrint(id+" "+point.toString()+" "+_point.toString());
      point+=_point;
      await Config.getFireStore().collection(COLLECTION_TEAMS).doc(id).update({'point': point});
    }
    catch(e){
      print(e);
      return null;
    }
  }


}