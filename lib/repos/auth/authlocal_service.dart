import 'package:hive/hive.dart';
import 'package:szsakd/model/user_model.dart';

class AuthLocalService{
  static const String BOX_KEY = 'user';

  static Future<void> saveUserLocal(FireStoreUser fireStoreUser) async {
    final box = await Hive.openBox(BOX_KEY);
    return box.put(fireStoreUser.uid, fireStoreUser.toMap());
  }

  static Future<FireStoreUser> getUserLocal(String uid) async {
    try {
      final box = await Hive.openBox(BOX_KEY);
      final boxValue = box.get(uid);
      if(boxValue != null){
        return FireStoreUser.fromMap(Map<String, dynamic>.from(boxValue));
      }
      return null;
    }
    catch(e){
      print(e);
      return null;
    }
  }
}