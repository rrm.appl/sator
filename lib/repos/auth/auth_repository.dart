import 'dart:async';
import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:szsakd/model/user_model.dart';
import 'package:szsakd/repos/auth/authlocal_service.dart';

class AuthRepository {

  static const String COLLECTION = "users";

  final FirebaseAuth firebaseAuth;
  AuthRepository({this.firebaseAuth});

  Future<User> signInWithCredentials(String email, String password) async {
    final result = await firebaseAuth.signInWithEmailAndPassword(email: email, password: password).catchError(
            (e){
          throw e.code;
        }
    );
    final User user = result.user;
    Config.currentFirebaseUser = user;
    debugPrint(email+" "+password);
    assert (user != null);
    assert (await user.getIdToken() != null);

    return user;
  }

  Future<User> createUserWithEmailAndPassword(email, password) async {
    final result = await firebaseAuth.createUserWithEmailAndPassword(email: email, password: password).catchError(
            (e){
              if(e.code == 'email-already-in-use'){
                throw e.code;
              }
            }
    );
    final User user = result.user;
    Config.currentFirebaseUser = user;
    await user.sendEmailVerification();

    assert (user != null);
    assert (await user.getIdToken() != null);

    return user;
  }

  Future<void> createUserIntoFireStore(FireStoreUser fireStoreUser) async {
    await AuthLocalService.saveUserLocal(fireStoreUser);
    debugPrint("firesotreuse: "+fireStoreUser.uid);
    return await Config.getFireStore().collection(COLLECTION).doc(fireStoreUser.uid).set(fireStoreUser.toMap()).catchError((e)=>debugPrint(e));
  }

  Future<FireStoreUser> readUserFromLocal(User user) async{
    return await AuthLocalService.getUserLocal(user.uid);
  }

  Future<void> saveUserFromLocal(FireStoreUser user) async{
    return await AuthLocalService.saveUserLocal(user);
  }

  Future<FireStoreUser> readUserFromCloud(String uid) async {
    FireStoreUser fireStoreUser = await Config.getFireStore().collection(COLLECTION).doc(uid).get().then(
            (value) => FireStoreUser.fromMap(value.data()))
        .catchError((e)=>debugPrint(e));
    return fireStoreUser;
  }

  Future<bool> refreshToken(FireStoreUser fireStoreUser) async{
    //token update
    String token = await Config.firebaseMessaging.getToken().then((value) => value);
    if(token!=fireStoreUser.fcm_token){
      fireStoreUser.fcm_token = token;
      await saveUserFromLocal(fireStoreUser);
      await Config.getFireStore().collection(COLLECTION).doc(fireStoreUser.uid).update({'fcm_token': token});
      return true;
    }
    return false;
  }

  Future<void> signOut() async {
    Config.currentUser = null;
    Config.currentFirebaseUser = null;
    return await firebaseAuth.signOut();
  }

  bool isSignedIn() {
    final currentUser = firebaseAuth.currentUser;
    currentUser.getIdToken().then((value) => log(value));
    return currentUser != null;
  }

  Future<User> getUser() async {
    //await firebaseAuth.signOut();
    return firebaseAuth.currentUser;
  }
}