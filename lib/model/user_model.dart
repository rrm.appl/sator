class FireStoreUser{
  String name;
  String teamId;
  String email;
  String uid;
  bool edit;
  String fcm_token;

  FireStoreUser({this.name,this.teamId,this.email,this.uid,this.edit,this.fcm_token});

  Map<String, dynamic> toMap(){
    return{
      'name': name,
      'teamId': teamId,
      'email': email,
      'uid': uid,
      'edit': edit,
      'fcm_token': fcm_token,
    };
  }

  factory FireStoreUser.fromMap(Map<String, dynamic> map) {
    return FireStoreUser(
        name: map['name'] as String,
        teamId: map['teamId'] as String,
        email: map['email'] as String,
        uid: map['uid'] as String,
        edit: map['edit'] as bool,
        fcm_token: map['fcm_token'] as String
    );
  }
}