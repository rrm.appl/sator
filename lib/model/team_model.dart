import 'package:cloud_firestore/cloud_firestore.dart';
class Team{
  String name;
  int point;
  int rank;
  String id;

  Team({this.name,this.point,this.rank,this.id});

  Map<String, dynamic> toMap(){
    return{
      'name': name,
      'point': point,
      'rank': rank,
      'id': id,
    };
  }

  factory Team.fromMap(Map<String, dynamic> map) {
    return Team(
        name: map['name'] as String,
        point: map['point'] as int,
        rank: map['rank'] as int,
        id: map['id'] as String,
    );
  }

  factory Team.fromCloudMap(QueryDocumentSnapshot map) {
    return Team(
      name: map.get('name') as String,
      point: map.get('point') as int,
      rank: map.get('rank') as int,
      id: map.get('id') as String,
    );
  }
}