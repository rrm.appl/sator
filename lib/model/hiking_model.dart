import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:szsakd/model/latilongi_model.dart';

class Hiking {
  String from;
  String to;
  double distance;
  int point;
  List<LatiLongi> trip;
  String teamId;

  Hiking({this.from, this.to, this.distance, this.point, this.trip, this.teamId});

  Map<String, dynamic> toMap(){
    return{
      'from': from,
      'to': to,
      'distance': distance,
      'point': point,
      'trip': trip != null ? trip.map((e) => e.toMap()).toList() : null,
      'teamId': teamId,
    };
  }

  factory Hiking.fromMap(Map<String, dynamic> map) {
    return Hiking(
        from: map['from'] as String,
        to: map['to'] as String,
        distance: map['distance'] as double,
        point: map['point'] as int,
        trip: map['trip'] !=null ?  List<dynamic>.from(map['trip']).map((e) => LatiLongi.fromMap(e)).toList() : null,
        teamId: map['teamId'] as String,
    );
  }

  factory Hiking.fromCloudMap(QueryDocumentSnapshot map) {
    return Hiking(
      from: map.get('from') as String,
      to: map.get('to') as String,
      distance: map.get('distance') as double,
      point: map.get('point') as int,
      trip: map.get('trip') !=null ?  List<dynamic>.from(map.get('trip')).map((e){
        return LatiLongi.fromMap(e);
      }).toList() : null,
      teamId: map.get('teamId') as String,
    );
  }

  @override
  String toString(){
    String tripStr = trip != null ? trip.length.toString() : '-';
    return '$from, $to, $distance, $point, $tripStr, $teamId';
  }
}