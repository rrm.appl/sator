import 'dart:ui';
import 'package:latlong/latlong.dart';
class CampMarker{
  LatLng position;
  Color color;
  CampMarker({this.position,this.color});
}