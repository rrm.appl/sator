class LatiLongi {
  final double latitude;
  final double longitude;
  LatiLongi({this.latitude, this.longitude});

  factory LatiLongi.fromMap(Map<dynamic, dynamic> map) => new LatiLongi(
      latitude: map['latitude'],
      longitude: map['longitude']);

  Map<String, dynamic> toMap(){
    return{
      'latitude': latitude,
      'longitude': longitude
    };
  }

  @override
  String toString() => 'LatiLongi $latitude: $longitude';
}