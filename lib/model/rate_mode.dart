class Rate{
  int score;
  int rated;
  Rate({this.rated,this.score});

  Map<String, dynamic> toMap(){
    return{
      'rated': rated,
      'score': score
    };
  }

  factory Rate.fromMap(Map<String, dynamic> map) {
    return Rate(
        rated: map['rated'] as int,
        score: map['score'] as int
    );
  }

  @override
  String toString() {
    return '$rated, $score';
  }
}