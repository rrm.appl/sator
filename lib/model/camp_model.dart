import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:szsakd/model/rate_mode.dart';
class Camp {
  String campName;
  double campName_lat;
  double campName_lon;
  String drinkWater;
  double drinkWater_lat;
  double drinkWater_lon;
  String bathWater;
  double bathWater_lat;
  double bathWater_lon;
  String shop;
  double shop_lat;
  double shop_lon;
  String playground;
  double playground_lat;
  double playground_lon;
  String price;
  String contact;
  Rate rate;
  String uid;


  Camp({this.campName,this.campName_lat,this.campName_lon,this.drinkWater,this.drinkWater_lat,this.drinkWater_lon,this.bathWater,this.bathWater_lat,this.bathWater_lon,this.contact,this.playground,this.playground_lat,this.playground_lon,this.price,this.shop,this.shop_lat,this.shop_lon, this.rate, this.uid});

  Map<String, dynamic> toMap(){
    return{
      'campName': campName,
      'campName_lon': campName_lon,
      'campName_lat': campName_lat,
      'drinkWater': drinkWater,
      'drinkWater_lat': drinkWater_lat,
      'drinkWater_lon': drinkWater_lon,
      'bathWater': bathWater,
      'bathWater_lat': bathWater_lat,
      'bathWater_lon': bathWater_lon,
      'shop': shop,
      'shop_lat': shop_lat,
      'shop_lon': shop_lon,
      'playground': playground,
      'playground_lat': playground_lat,
      'playground_lon': playground_lon,
      'price': price,
      'contact': contact,
      'rate': rate.toMap(),
      'uid': uid
    };
  }

  factory Camp.fromMap(Map<String, dynamic> map) {
    return Camp(
        campName: map['campName'] as String,
        campName_lon: map['campName_lon'] as double,
        campName_lat: map['campName_lat'] as double,
        drinkWater: map['drinkWater'] as String,
        drinkWater_lat: map['drinkWater_lat'] as double,
        drinkWater_lon: map['drinkWater_lon'] as double,
        bathWater: map['bathWater'] as String,
        bathWater_lat: map['bathWater_lat'] as double,
        bathWater_lon: map['bathWater_lon'] as double,
        shop: map['shop'] as String,
        shop_lat: map['shop_lat'] as double,
        shop_lon: map['shop_lon'] as double,
        playground: map['playground'] as String,
        playground_lat: map['playground_lat'] as double,
        playground_lon: map['playground_lon'] as double,
        price: map['price'] as String,
        contact: map['contact'] as String,
      rate: Rate.fromMap(map['rate']),
      uid: map['uid'] as String
    );
  }

  factory Camp.fromCloudMap(QueryDocumentSnapshot map) {
    return Camp(
        campName: map.get('campName') as String,
        campName_lon: map.get('campName_lon') as double,
        campName_lat: map.get('campName_lat') as double,
        drinkWater: map.get('drinkWater') as String,
        drinkWater_lat: map.get('drinkWater_lat') as double,
        drinkWater_lon: map.get('drinkWater_lon') as double,
        bathWater: map.get('bathWater') as String,
        bathWater_lat: map.get('bathWater_lat') as double,
        bathWater_lon: map.get('bathWater_lon') as double,
        shop: map.get('shop') as String,
        shop_lat: map.get('shop_lat') as double,
        shop_lon: map.get('shop_lon') as double,
        playground: map.get('playground') as String,
        playground_lat: map.get('playground_lat') as double,
        playground_lon: map.get('playground_lon') as double,
        price: map.get('price') as String,
        contact: map.get('contact') as String,
      rate: Rate.fromMap(map.get('rate')),
      uid: map.get('uid') as String
    );
  }

  @override
  String toString() {
    return '$campName, $campName_lat, $campName_lon, $drinkWater, $drinkWater_lat, $drinkWater_lon, $bathWater, $bathWater_lat, $bathWater_lon, $shop, $shop_lat, $shop_lon, $playground, $playground_lat, $playground_lon $price $contact ${rate.toString()}';
  }
}