import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:szsakd/model/user_model.dart';
import 'package:szsakd/utils/notification.dart' as Notify;

class Config{
  static FireStoreUser currentUser;
  static User currentFirebaseUser;
  static FirebaseFirestore firebaseFirestore;
  static FirebaseMessaging firebaseMessaging;
  static FirebaseStorage firebaseStorage;
  static Notify.Notification notification;

  static Future<void> initFlutter() async{
    await Hive.initFlutter('localdb');
  }

  static Future<void> initFirebase() async{
    await Firebase.initializeApp();
  }

  static FirebaseAuth getFirebaseAuth(){
    FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
    return _firebaseAuth;
  }

  static void setFireStorePersistence(bool enabled){
    firebaseFirestore = FirebaseFirestore.instance;
    firebaseFirestore.settings = Settings(persistenceEnabled: enabled);
  }

  static FirebaseFirestore getFireStore(){
    return firebaseFirestore;
  }

  static void setFirebaseMessaging(){
    firebaseMessaging = FirebaseMessaging();
  }
  static FirebaseMessaging getFirebaseMessaing(){
    return firebaseMessaging;
  }
  static void setFirebaseStorage(){
    firebaseStorage = FirebaseStorage.instance;
  }

  static void setNotification(){
    notification = Notify.Notification();
  }

}