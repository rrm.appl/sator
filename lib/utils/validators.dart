class Validators {
  static final Pattern emailPattern = r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+.[a-zA-Z0-9-]*$';
  static final Pattern passwPattern =  r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$';


  static bool isValidEmail(String email) {
    RegExp regex = new RegExp(emailPattern);
    if(!regex.hasMatch(email))
      return false;
    return true;
  }

  static bool isValidPassword(String password) {
    RegExp regex = new RegExp(passwPattern);
    if (!regex.hasMatch(password))
        return false;
    return true;
  }
}