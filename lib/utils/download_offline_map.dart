import 'dart:io';

import 'package:flutter/material.dart';
import 'package:szsakd/config/config_app.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart' as pathProvider;
import 'package:firebase_storage/firebase_storage.dart';

class DownloadOfflineMap{
  static Future<bool> downloadURLExample(String fileName) async {
    Reference ref = await Config.firebaseStorage.ref('/$fileName');
    debugPrint("start "+ref.toString());
    //String downloadURL = await Config.firebaseStorage.ref('/$fileName').getDownloadURL();
    //final http.Response downloadData = await http.get(downloadURL);
    final externalDirectory = await pathProvider.getExternalStorageDirectory();
    File tempFile = File('${externalDirectory.path}/$fileName');
    if (tempFile.existsSync()) {
      await tempFile.delete();
    }
    await tempFile.create();
    debugPrint("file "+tempFile.path);
    await ref.writeToFile(tempFile);

    debugPrint("downloaded ");
  }

  static Future<bool> existsMap(String fileName) async {
    final externalDirectory = await pathProvider.getExternalStorageDirectory();
    File tempFile = File('${externalDirectory.path}/$fileName');
    if (tempFile.existsSync()) {
      return true;
    }
    return false;
  }
}