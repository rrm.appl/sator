import 'dart:ui';

const PRIMARY_COLOR  = Color(0xFF232B34);
const TENT_COLOR = Color(0xFF23D1F9);
const DISABLE_COLOR = Color(0xFF232356);
const WHITE_COLOR = Color(0xFFFFFFFF);
const WHITER_COLOR = Color(0xFFC5C5C5);

const ORANGE_COLOR = Color(0xFFFB8C00);
const GREEN_COLOR = Color(0xFF43A047);
const BLUE_COLOR = Color(0xFF1E88E5);
const ACCENTBLUE_COLOR = Color(0xFF2962FF);
const BLACK_COLOR = Color(0xFF000000);
const RED_COLOR = Color(0xFFE53935);

const BLACK12_COLOR = Color(0x1F000000);
const GREY_COLOR = Color(0xFF757575);

