
class Converter{
  static const kmPerPoint = 5;
  static const pointPerCamp = 5;

  static double meterToKm(double meter){
    return meter/1000;
  }

  static int kmToPoint(double km){
    return (km ~/ kmPerPoint);
  }
}