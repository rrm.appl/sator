import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;
import 'package:sqflite/sqflite.dart';

class TileDownload{

  static Future<void> download(int zoom) async{
    final externalDirectory = await pathProvider.getExternalStorageDirectory();
    File f = File('${externalDirectory.path}/maptiles.db');
    Database database = await openDatabase('${externalDirectory.path}/maptiles.db', version: 1,
        onCreate: (Database db, int version) async {
          await db.execute(
              "CREATE TABLE tiles (id  INTEGER PRIMARY KEY, zoom_level  INTEGER, tile_column INTEGER, tile_row INTEGER, tile_data BLOB)");
        });
    //for(int i=8;i<=13;i++) {
      int i=zoom;
      log('Start download: ' + i.toString());
      StartEndPoint sp = getStartEndPoint(i);
      for (int x = sp.sx; x <= sp.ex; x++) {
        for (int y = sp.sy; y <= sp.ey; y++) {
          String url = 'https://tiles.wmflabs.org/hikebike/' + i.toString() + '/' + x.toString() + '/' + y.toString() + '.png';
          final ByteData imageData = await NetworkAssetBundle(Uri.parse(url)).load("");
          final Uint8List bytes = imageData.buffer.asUint8List();
          final stringFromBytes = json.encode(bytes);
          await database.transaction((txn) async {
            int id = await txn.rawInsert(
                'INSERT INTO tiles(zoom_level, tile_column, tile_row, tile_data) VALUES(?, ?, ?, ?)',
                [i, x, y, stringFromBytes]);
            print('inserted ($i): $id');
          });
        }
      //}
    }
    log('Start end: '+i.toString());
    await database.close();
  }
  static getSize(List<int> zooms, Directory directory) async{
    double all = 0;
    for(int zoom in zooms) {
      double zoom_ = 0;
      StartEndPoint sp = getStartEndPoint(zoom);
      for (int x = sp.sx; x <= sp.ex; x++) {
        for (int y = sp.sy; y <= sp.ey; y++) {
          await File('${directory.path}/' + zoom.toString() + '/' + x.toString() + '/' + y.toString() + '.png').length().then((value) {
            zoom_ += value;
          });
        }
      }
      all+=zoom_;
      log(zoom.toString()+" : "+zoom_.toString() + " byte");
    }
    log("Size: "+all.toString());
  }
}

StartEndPoint getStartEndPoint(int zoom){
  switch(zoom){
    case 8:
      return StartEndPoint(139, 144, 88, 91);
      break;
    case 9:
      return StartEndPoint(278, 288, 176, 182);
      break;
    case 10:
      return StartEndPoint(557, 577, 353, 365);
      break;
    case 11:
      return StartEndPoint(1115, 1154, 706, 730);
      break;
    case 12:
      return StartEndPoint(2231, 2308, 1413 , 1461);
      break;
    case 13:
      return StartEndPoint(4462, 4617, 2827 , 2923);
      break;
    case 14:
      return StartEndPoint(8925, 9234 , 5655 , 5846);
      break;
    case 15:
      return StartEndPoint(17850, 18468, 11310 , 11692);
      break;
    default:
      return StartEndPoint(0, 0, 0, 0);
      break;
  }
}


class StartEndPoint{
  int sx;
  int ex;
  int sy;
  int ey;
  StartEndPoint(int sx, int ex, int sy, int ey){
    this.sx=sx;
    this.ex=ex;
    this.sy=sy;
    this.ey=ey;
  }
}